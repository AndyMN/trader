from ExchangeAPIs.Binance import Binance, ExecutionOrderType, UserDataStreamType, read_binance_user_data_stream, user_data_stream_to_human_text, KlineInterval, OrderStatus, OrderSide
from Positions.DBPositionModels import Base
from PositionManagers.PositionManagerBinance import PositionManagerBinance
from Utils.telegrambot import send_telegram_message
from Utils.utils import rewrite_binance_kline_data
from TickingDataSources.TickingDataSourceWebSocket import TickingDataSourceWebSocket
import secret_keys

from sqlalchemy import create_engine

from queue import Queue
import time
import logging


def on_tick(tick_data):
    def process_tick():
        kline_data = rewrite_binance_kline_data(tick_data["k"])
        print(kline_data)
        if tick_data["k"]["x"]:
            print("Kline is closed !")

            close_price = float(kline_data["close_price"])
            position_manager.try_open_long_position(price_data=kline_data, capital_fraction=0.02,
                                                    profit_target_price=1.01 * close_price,
                                                    stop_loss_trigger_price=0.99 * close_price,
                                                    stop_loss_price=0.95 * close_price)

    tasks.put(process_tick)



def on_user_data_tick(tick_data=None):

    def process_user_data_tick():
        
        user_data_tick = read_binance_user_data_stream(tick_data=tick_data)

        print(user_data_stream_to_human_text(tick_data=tick_data))
        logging.info(f"User data stream: {user_data_stream_to_human_text(tick_data=tick_data)}")

        if enable_telegram:
            send_telegram_message(user_data_stream_to_human_text(tick_data=tick_data))

        if user_data_tick["event_type"] == UserDataStreamType.EXECUTION_REPORT.value:
            if user_data_tick["side"] == OrderSide.SELL.value:
                if user_data_tick["current_execution_type"] == ExecutionOrderType.TRADE.value:
                    if user_data_tick["current_order_status"] in [OrderStatus.FILLED.value, OrderStatus.PARTIALLY_FILLED.value]:
                        position_manager.fill_open_long_position(order_id=int(user_data_tick["order_id"]),
                                                                fill_amount=float(user_data_tick["cumulative_filled_quantity"]),
                                                                fill_value=float(user_data_tick["cumulative_quote_asset_transacted_quantity"]),
                                                                fill_time=int(user_data_tick["transaction_time"]))
    tasks.put(process_user_data_tick)


if __name__ == "__main__":

    # USER CONFIG
    enable_telegram = True
    enable_sqlitedb = True
    base_currency = "ETH"
    quote_currency = "USDT"
    ##########################

    logging.basicConfig(level=logging.INFO, filename=f"mainTestnet_" + base_currency + quote_currency + ".log", 
                        format='%(process)d-%(asctime)s-%(levelname)s-%(message)s')


    bot_creation_time = time.time()

    # Initialize Binance API object
    test_endpoint = "testnet.binance.vision"
    binance = Binance(api_endpoint=f"https://{test_endpoint}", 
                    api_key=secret_keys.binance_test_apikey, 
                    api_secret=secret_keys.binance_test_secret)
    tasks = Queue()

    # Initialize sqlite DB
    if enable_sqlitedb:
        testnet_engine = create_engine("sqlite+pysqlite:///mainTestnet_" + base_currency + quote_currency + ".db", echo=True)
        Base.metadata.create_all(testnet_engine)
    db_engine = testnet_engine if enable_sqlitedb else None

    # Initialize Binance position manager
    position_manager = PositionManagerBinance(binance=binance, wallet_asset=quote_currency, 
                                              n_position_limit=5, db_engine=db_engine, 
                                              enable_telegram=enable_telegram)

    # Initialize user data stream listener (receives changes on the user account and orders)
    binance_user_data_stream_listenkey = binance.create_spot_listen_key()["listenKey"]
    listen_key_start_time = time.time()
    binance_user_data_stream = f"wss://{test_endpoint}/ws/" + binance_user_data_stream_listenkey
    binance_user_data_stream_ticker = TickingDataSourceWebSocket(stream_url=binance_user_data_stream)
    binance_user_data_stream_ticker.attach_fun_to_tick(on_user_data_tick)
    binance_user_data_stream_ticker.start()

    # Initialize the kline ticker for the defined currency pair and ticker interval
    symbol = base_currency + quote_currency
    binance_test_stream = f"wss://{test_endpoint}/ws/{symbol.lower()}@kline_{KlineInterval.ONE_MINUTE.value}"
    binance_ticker = TickingDataSourceWebSocket(stream_url=binance_test_stream)
    binance_ticker.attach_fun_to_tick(on_tick)
    binance_ticker.start()

    while True:
        try:
            tasks.get_nowait()()
        except:
            pass
        
        # Every 30 mins refresh the user data stream listen key. It expires after 1 hour
        if time.time() - listen_key_start_time >= 30 * 60:
            binance.keep_alive_spot_listen_key(listenKey=binance_user_data_stream_listenkey)
            listen_key_start_time = time.time()
