import requests
import hmac
import hashlib
import time
import secrets
from urllib.parse import urlencode
import math
from datetime import datetime

from enum import Enum

'''

Spot API URL	Spot Test Network URL
https://api.binance.com/api	https://testnet.binance.vision/api
wss://stream.binance.com:9443/ws	wss://testnet.binance.vision/ws
wss://stream.binance.com:9443/stream	wss://testnet.binance.vision/stream

'''


class SymbolStatus(Enum):
    # status
    PRE_TRADING = "PRE_TRADING"
    TRADING = "TRADING"
    POST_TRADING = "POST_TRADING"
    END_OF_DAY = "END_OF_DAY"
    HALT = "HALT"
    AUCTION_MATCH = "AUCTION_MATCH"
    BREAK = "BREAK"

class AccountSymbolPermissions(Enum):
    # permissions
    SPOT = "SPOT"
    MARGIN = "MARGIN"
    LEVERAGED = "LEVERAGED"
    TRD_GRP_002 = "TRD_GRP_002"

class OrderStatus(Enum):
    # status
    NEW = "NEW" # Order has been accepted by the engine
    PARTIALLY_FILLED = "PARTIALLY_FILLED" # A part of the order has been filled
    FILLED = "FILLED" # The order has been completed
    CANCELED = "CANCELED" # the order has been canceled by the user
    PENDING_CANCEL = "PENDING_CANCEL" # Currently unused
    REJECTED = "REJECTED" # The order was not accepted by the engine and not processed
    EXPIRED = "EXPIRED" # The order was canceled according to the order type's rules (e.g. LIMIT FOK orders with no fill, LIMIT IOC or MARKET order that partially fill) or by the exchange, (e.g. orders canceled during liquidation, orders canceled during maintenance)

class OCOStatus(Enum):
    # listStatusType
    RESPONSE = "RESPONSE" # This is used when the ListStatus is reponding to a faield action. (e.g. Orderlist placement or cancellation)
    EXEC_STARTED = "EXEC_STARTED" # The order list has been placed or there is an update to the order list status
    ALL_DONE = "ALL_DONE" # The order list has finished executing and thus no longer active

class OCOOrderStatus(Enum):
    # listOrderStatus
    EXECUTING = "EXECUTING" # Either an order list has been placed or there is an update to the status of the list
    ALL_DONE = "ALL_DONE" # An order list has completed execution and thus no longer active
    REJECT = "REJECT" # The List Status is responding to a failed action either during order placement or order canceled.

class ContingencyType(Enum):
    OCO = "OCO"

class OrderType(Enum):
    # orderTypes, type
    LIMIT = "LIMIT"
    MARKET = "MARKET"
    STOP_LOSS = "STOP_LOSS"
    STOP_LOSS_LIMIT = "STOP_LOSS_LIMIT"
    TAKE_PROFIT = "TAKE_PROFIT"
    TAKE_PROFIT_LIMIT = "TAKE_PROFIT_LIMIT"
    LIMIT_MAKER = "LIMIT_MAKER"

class OrderResponseType(Enum):
    # newOrderRespType
    ACK = "ACK"
    RESULT = "RESULT"
    FULL = "FULL"

class OrderSide(Enum):
    # side
    BUY = "BUY"
    SELL = "SELL"

class TimeInForce(Enum):
    # timeInForce
    # This sets how long an order will be active before expiration
    GTC = "GTC" # Good Til Canceled. An order will be on the book unless the order is canceled
    IOC = "IOC" # Immediate or Cancel. An order will try to fill the order as much as it can before the order expires
    FOK = "FOK" # Fill or Kill. An order will expire if the full order cannot be filled upon execution

class KlineInterval(Enum):
    ONE_MINUTE = "1m"
    THREE_MINUTES = "3m"
    FIVE_MINUTES = "5m"
    FIFTEEN_MINUTES = "15m"
    THIRTY_MINUTES = "30m"
    ONE_HOUR = "1h"
    TWO_HOURS = "2h"
    FOUR_HOURS = "4h"
    SIX_HOURS = "6h"
    EIGHT_HOURS = "8h"
    TWELVE_HOURS = "12h"
    ONE_DAY = "1d"
    THREE_DAYS = "3d"
    ONE_WEEK = "1w"
    ONE_MONTH = "1M"

class RateLimiters(Enum):
    # rateLimitType
    REQUEST_WEIGHT = "REQUEST_WEIGHT"
    ORDERS = "ORDERS"
    RAW_REQUESTS = "RAW_REQUESTS"

class RateLimitInterval(Enum):
    # interval
    SECOND = "SECOND"
    MINUTE = "MINUTE"
    DAY = "DAY"

class SymbolFilter(Enum):
    '''
    The PRICE_FILTER defines the price rules for a symbol. There are 3 parts:

    minPrice defines the minimum price/stopPrice allowed; disabled on minPrice == 0.
    maxPrice defines the maximum price/stopPrice allowed; disabled on maxPrice == 0.
    tickSize defines the intervals that a price/stopPrice can be increased/decreased by; disabled on tickSize == 0.

    Any of the above variables can be set to 0, which disables that rule in the price filter. 
    
    In order to pass the price filter, the following must be true for price/stopPrice of the enabled rules:

        price >= minPrice
        price <= maxPrice
        price % tickSize == 0
    '''
    PRICE_FILTER = "PRICE_FILTER"

    '''
    The PERCENT_PRICE filter defines valid range for a price based on the average of the previous trades. avgPriceMins is the number of minutes the average price is calculated over. 0 means the last price is used.

    In order to pass the percent price, the following must be true for price:

        price <= weightedAveragePrice * multiplierUp
        price >= weightedAveragePrice * multiplierDown
    '''
    PERCENT_PRICE = "PERCENT_PRICE"

    '''
    The PERCENT_PRICE_BY_SIDE filter defines the valid range for the price based on the lastPrice of the symbol. There is a different range depending on whether the order is placed on the BUY side or the SELL side.

    Buy orders will succeed on this filter if:

        Order price <= bidMultiplierUp * lastPrice
        Order price >= bidMultiplierDown * lastPrice

    Sell orders will succeed on this filter if:

        Order Price <= askMultiplierUp * lastPrice
        Order Price >= askMultiplierDown * lastPrice
    '''
    PERCENT_PRICE_BY_SIDE = "PERCENT_PRICE_BY_SIDE"
    
    '''
    The LOT_SIZE filter defines the quantity (aka "lots" in auction terms) rules for a symbol. 
    There are 3 parts:

        minQty defines the minimum quantity/icebergQty allowed.
        maxQty defines the maximum quantity/icebergQty allowed.
        stepSize defines the intervals that a quantity/icebergQty can be increased/decreased by.

    In order to pass the lot size, the following must be true for quantity/icebergQty:

        quantity >= minQty
        quantity <= maxQty
        (quantity-minQty) % stepSize == 0
    '''
    LOT_SIZE = "LOT_SIZE"

    '''
    The MIN_NOTIONAL filter defines the minimum notional value allowed for an order on a symbol. 
    An order's notional value is the price * quantity. 
    If the order is an Algo order (e.g. STOP_LOSS_LIMIT), then the notional value of the stopPrice * quantity will also be evaluated. 
    If the order is an Iceberg Order, then the notional value of the price * icebergQty will also be evaluated. 
    applyToMarket determines whether or not the MIN_NOTIONAL filter will also be applied to MARKET orders. 
    Since MARKET orders have no price, the average price is used over the last avgPriceMins minutes. 
    avgPriceMins is the number of minutes the average price is calculated over. 
    0 means the last price is used.
    '''
    MIN_NOTIONAL = "MIN_NOTIONAL"

    '''
    The ICEBERG_PARTS filter defines the maximum parts an iceberg order can have. 
    The number of ICEBERG_PARTS is defined as CEIL(qty / icebergQty).
    '''
    ICEBERG_PARTS = "ICEBERG_PARTS"

    '''
    The MARKET_LOT_SIZE filter defines the quantity (aka "lots" in auction terms) rules for MARKET orders on a symbol. 
    There are 3 parts:

        minQty defines the minimum quantity allowed.
        maxQty defines the maximum quantity allowed.
        stepSize defines the intervals that a quantity can be increased/decreased by.
    
    In order to pass the market lot size, the following must be true for quantity:

        quantity >= minQty
        quantity <= maxQty
        (quantity-minQty) % stepSize == 0
    '''
    MARKET_LOT_SIZE = "MARKET_LOT_SIZE"

    '''
    The MAX_NUM_ORDERS filter defines the maximum number of orders an account is allowed to have open on a symbol. 
    Note that both "algo" orders and normal orders are counted for this filter.
    '''
    MAX_NUM_ORDERS = "MAX_NUM_ORDERS"

    '''
    The MAX_NUM_ALGO_ORDERS filter defines the maximum number of "algo" orders an account is allowed to have open on a symbol. 
    "Algo" orders are STOP_LOSS, STOP_LOSS_LIMIT, TAKE_PROFIT, and TAKE_PROFIT_LIMIT orders.
    '''
    MAX_NUM_ALGO_ORDERS = "MAX_NUM_ALGO_ORDERS"

    '''
    The MAX_NUM_ICEBERG_ORDERS filter defines the maximum number of ICEBERG orders an account is allowed to have open on a symbol. 
    An ICEBERG order is any order where the icebergQty is > 0.
    '''
    MAX_NUM_ICEBERG_ORDERS = "MAX_NUM_ICEBERG_ORDERS"

    '''
    The MAX_POSITION filter defines the allowed maximum position an account can have on the base asset of a symbol.
    An account's position defined as the sum of the account's:

        free balance of the base asset
        locked balance of the base asset
        sum of the qty of all open BUY orders

    BUY orders will be rejected if the account's position is greater than the maximum position allowed.
    '''
    MAX_POSITION = "MAX_POSITION"

    '''
    The TRAILING_DELTA filter defines the minimum and maximum value for the parameter trailingDelta.

    In order for a trailing stop order to pass this filter, the following must be true:

        For STOP_LOSS BUY, STOP_LOSS_LIMIT_BUY,TAKE_PROFIT SELL and TAKE_PROFIT_LIMIT SELL orders:

            trailingDelta >= minTrailingAboveDelta
            trailingDelta <= maxTrailingAboveDelta
        
        For STOP_LOSS SELL, STOP_LOSS_LIMIT SELL, TAKE_PROFIT BUY, and TAKE_PROFIT_LIMIT BUY orders:

            trailingDelta >= minTrailingBelowDelta
            trailingDelta <= maxTrailingBelowDelta
    '''
    TRAILING_DELTA = "TRAILING_DELTA"

class ExchangeFilter(Enum):
    '''
    The EXCHANGE_MAX_NUM_ORDERS filter defines the maximum number of orders an account is allowed to have open on the exchange. 
    Note that both "algo" orders and normal orders are counted for this filter.
    '''
    EXCHANGE_MAX_NUM_ORDERS = "EXCHANGE_MAX_NUM_ORDERS"

    '''
    The EXCHANGE_MAX_NUM_ALGO_ORDERS filter defines the maximum number of "algo" orders an account is allowed to have open on the exchange. 
    "Algo" orders are STOP_LOSS, STOP_LOSS_LIMIT, TAKE_PROFIT, and TAKE_PROFIT_LIMIT orders.
    '''
    EXCHANGE_MAX_NUM_ALGO_ORDERS = "EXCHANGE_MAX_NUM_ALGO_ORDERS"

class ExecutionOrderType(Enum):
    NEW = "NEW"  # The order has been accepted into the engine
    CANCELED = "CANCELED"  # The order has been canceled by the user
    TRADE = "TRADE"  # Part of the order or all of the order's quantity has filled

    '''
    The order was canceled according to the order type's rules (e.g. LIMIT FOK order with no fill, 
    LIMIT IOC or MARKET order that partially fill) or by the exchange (e.g order canceled during liquidation,
    orders canceled during maintenance)
    '''
    EXPIRED = "EXPIRED"

    REPLACED = "REPLACED"  # Currently unused
    REJECTED = "REJECTED" # The order has been rejected and was not processed (Not pushed into the User Data Stream)


class Binance:
    def __init__(self, api_endpoint: str, api_key: str, api_secret: str):
        self.api_endpoint = api_endpoint
        self.api_key = api_key
        self.api_secret = api_secret

        self.symbol_information = {}
        
        self.session = self._prepare_session()
    

    def _prepare_session(self):

        s = requests.Session()
        headers = {"Content-Type": "application/json;charset=utf-8", "X-MBX-APIKEY": self.api_key}
        s.headers.update(headers)

        #retries = Retry(total=20, backoff_factor=0.1, status_forcelist=[400])
        #s.mount("https://", HTTPAdapter(max_retries=retries))

        return s
    
    def _get_timestamp(self):
        return int(time.time() * 1000)
    
    def _send_request(self, http_method: str, complete_endpoint: str, payload: dict):
        # For NONE, USER_STREAM, and MARKET_DATA security types

        query_string = urlencode(payload, True)
        
        url = complete_endpoint + "?" + query_string

        request = requests.Request(method=http_method, url=url)
        prepped = self.session.prepare_request(request=request)
        response = self.session.send(request=prepped)
        
        response.raise_for_status()
        
        return response.json()

    def _send_signed_request(self, http_method: str, complete_endpoint: str, payload: dict):
        # For TRADE, USER_DATA, and MARGIN security types

        payload["timestamp"] = self._get_timestamp()

        query_string = urlencode(payload, True)
        
        signature = hmac.new(self.api_secret.encode("utf-8"), query_string.encode("utf-8"), digestmod=hashlib.sha256).hexdigest()

        url = complete_endpoint + "?" + query_string + "&signature=" + signature

        request = requests.Request(method=http_method, url=url)
        prepped = self.session.prepare_request(request=request)
        response = self.session.send(request=prepped)
        
        response.raise_for_status()

        return response.json()
       
    
    def _get_market_data(self, partial_endpoint: str, payload: dict):

        http_method = "get"
        complete_endpoint = self.api_endpoint + partial_endpoint

        response = self._send_request(http_method=http_method, complete_endpoint=complete_endpoint, payload=payload)

        return response
    
    def _get_symbol_information(self, symbol: str):
        
        baseAssetPrecision = self.get_exchange_info(symbol)["symbols"][0]["baseAssetPrecision"]
        quoteAssetPrecision = self.get_exchange_info(symbol)["symbols"][0]["quoteAssetPrecision"]
        stepSize = float(self.get_symbol_filters(symbol)[SymbolFilter.LOT_SIZE.value]["stepSize"])
        tickSize = float(self.get_symbol_filters(symbol)[SymbolFilter.PRICE_FILTER.value]["tickSize"])

        self.symbol_information[symbol] = {"baseAssetPrecision": baseAssetPrecision, "quoteAssetPrecision": quoteAssetPrecision,
                                           "stepSize": stepSize, "tickSize": tickSize}

    ########## WALLET ENDPOINT #####

    def get_trade_fee(self, symbol: str, recvWindow: int=5000):

        payload = {"recvWindow": recvWindow}

        if symbol:
            payload["symbol"] = symbol
        
        http_method = "get"
        complete_endpoint = self.api_endpoint + "/sapi/v1/asset/tradeFee"

        return self._send_signed_request(http_method=http_method, complete_endpoint=complete_endpoint, payload=payload)

    ########## MARKET DATA ##########

    def get_server_time(self):

        payload = {}

        return self._get_market_data(partial_endpoint="/api/v3/time", payload=payload)
    
    def get_exchange_info(self, symbol: str=None):
        
        payload = {}
        
        if symbol:
            payload["symbol"] = symbol
        
        return self._get_market_data(partial_endpoint="/api/v3/exchangeInfo", payload=payload)
    
    def get_symbol_filters(self, symbol: str):

        response = self.get_exchange_info(symbol=symbol)

        filters = response["symbols"][0]["filters"]

        filters_dict = {}
        for filter in filters:
            filters_dict[filter["filterType"]] = {}
            for filter_key, filter_val in filter.items():
                if not filter_key == "filterType":
                    filters_dict[filter["filterType"]][filter_key] = filter_val

        return filters_dict

    def get_order_book(self, symbol: str, limit: int=None):

        payload = {"symbol": symbol}

        if limit and limit > 0:
            payload["limit"] = limit
        
        return self._get_market_data(partial_endpoint="/api/v3/depth", payload=payload)

    def get_recent_trades_list(self, symbol: str, limit: int=None):

        payload = {"symbol": symbol}

        if limit and limit > 0:
            payload["limit"] = limit
        
        return self._get_market_data(partial_endpoint="/api/v3/trades", payload=payload)

    def get_compressed_aggregate_trades_list(self, symbol: str, fromId: int=None, startTime: int=None, endTime: int=None, limit: int=None):

        payload = {"symbol": symbol}

        if startTime and endTime:
            # If both are passed the time between them has to be less than 1 hour (they are timestamps in ms)
            if endTime - startTime < 1 * 60 * 60 * 1000:
                payload["startTime"] = startTime
                payload["endTime"] = endTime
        elif startTime:
            payload["startTime"] = startTime
        elif endTime:
            payload["endTime"] = endTime
        
        if fromId:
            payload["fromId"] = fromId
        
        if limit and limit < 1000:
            payload["limit"] = limit
        
        return self._get_market_data(partial_endpoint="/api/v3/aggTrades", payload=payload)

    def get_kline_data(self, symbol: str, interval: KlineInterval, startTime: int=None, endTime: int=None, limit: int=None):

        payload = {"symbol": symbol, "interval": interval.value}

        if startTime:
            payload["startTime"] = startTime

        if endTime:
            payload["endTime"] = endTime
        
        if limit and limit < 1000:
            payload["limit"] = limit
        
        return self._get_market_data(partial_endpoint="/api/v3/klines", payload=payload)

    def get_current_average_price(self, symbol: str):

        payload = {"symbol": symbol}

        return self._get_market_data(partial_endpoint="/api/v3/avgPrice", payload=payload)
    
    def get_24h_ticker_price_change_stats(self, symbol: str=None):

        payload = {}

        if symbol:
            payload["symbol"] = symbol
        
        return self._get_market_data(partial_endpoint="/api/v3/ticker/24hr", payload=payload)

    def get_symbol_price_ticker(self, symbol: str=None):

        payload = {}

        if symbol:
            payload["symbol"] = symbol
        
        return self._get_market_data(partial_endpoint="/api/v3/ticker/price", payload=payload)
    
    def get_symbol_order_book_ticker(self, symbol: str=None):

        payload = {}

        if symbol:
            payload["symbol"] = symbol
        
        return self._get_market_data(partial_endpoint="/api/v3/ticker/bookTicker", payload=payload)


    ########## USER DATA ############
    def query_order(self, symbol: str, orderId: int=None, origClientOrderId: str=""):
        if not orderId and not origClientOrderId:
            print("Need to pass orderID or origClientOrderId to query order !")
            exit(-1)
        
        http_method = "get"
        complete_endpoint = self.api_endpoint + "/api/v3/order"

        payload = {"symbol": symbol}
        if orderId:
            payload["orderId"] = orderId
        else:
            payload["origClientOrderId"] = origClientOrderId
        
        response = self._send_signed_request(http_method=http_method, complete_endpoint=complete_endpoint, payload=payload)
        
        return response
    
    def current_open_orders(self, symbol:str=None, recvWindow: int=5000):

        payload = {"recvWindow": recvWindow}

        if symbol:
            payload["symbol"] = symbol

        http_method = "get"
        complete_endpoint = self.api_endpoint + "/api/v3/openOrders"
        
        response = self._send_signed_request(http_method=http_method, complete_endpoint=complete_endpoint, payload=payload)

        return response
    
    def all_orders(self, symbol: str, orderId: int=None, startTime: int=None, endTime: int=None, limit: int=500, recvWindow: int=5000):
        payload = {"symbol": symbol, "limit": limit, "recvWindow": recvWindow}

        if startTime and endTime:
            payload["startTime"] = startTime
            payload["endTime"] = endTime
        elif orderId:
            payload["orderId"] = orderId
        
        http_method = "get"
        complete_endpoint = self.api_endpoint + "/api/v3/allOrders"
        
        response = self._send_signed_request(http_method=http_method, complete_endpoint=complete_endpoint, payload=payload)

        return response

    def query_oco(self, orderListId: int=None, origClientOrderId: str=None, recvWindow: int=None):
        
        payload = {}

        if not orderListId and not origClientOrderId:
            print("OrderListId or OrigClientOrderId has to be provided !")
            exit(-1)
        
        if orderListId:
            payload["orderListId"] = orderListId
        
        if origClientOrderId:
            payload["origClientOrderId"] = origClientOrderId
        
        if recvWindow and recvWindow < 60000:
            payload["recvWindow"] = recvWindow
        
        http_method = "get"
        complete_endpoint = self.api_endpoint + "/api/v3/orderList"

        response = self._send_signed_request(http_method=http_method, complete_endpoint=complete_endpoint, payload=payload)

        return response
    
    def query_all_oco(self, fromId: int=None, startTime: int=None, endTime: int=None, limit: int=None, recvWindow: int=None):

        payload = {}

        if fromId and (startTime or endTime):
            print("If fromId is supplied neither startTime or endTime can be provided")
            exit(-1)
        else:
            payload["fromId"] = fromId
       
        if startTime:
            payload["startTime"] = startTime
        
        if endTime:
            payload["endTime"] = endTime
        
        if limit and limit < 1000:
            payload["limit"] = limit
        
        if recvWindow and recvWindow < 60000:
            payload["recvWindow"] = recvWindow
        
        http_method = "get"
        complete_endpoint = self.api_endpoint + "/api/v3/allOrderList"

        response = self._send_signed_request(http_method=http_method, complete_endpoint=complete_endpoint, payload=payload)

        return response
    
    def query_open_oco(self, recvWindow: int=None):

        payload = {}

        if recvWindow and recvWindow < 60000:
            payload["recvWindow"] = recvWindow
        
        http_method = "get"
        complete_endpoint = self.api_endpoint + "/api/v3/openOrderList"

        response = self._send_signed_request(http_method=http_method, complete_endpoint=complete_endpoint, payload=payload)

        return response

    def get_account_info(self, recvWindow: int=None):
        
        payload = {}

        if recvWindow and recvWindow < 60000:
            payload["recvWindow"] = recvWindow

        http_method = "get"
        complete_endpoint = self.api_endpoint + "/api/v3/account"

        response = self._send_signed_request(http_method=http_method, complete_endpoint=complete_endpoint, payload=payload)

        return response
    
    def get_account_trade_list(self, symbol: str, orderId: int=None, startTime: int=None, endTime: int=None, fromId: int=None, limit: int=None, recvWindow: int=None):

        payload = {"symbol": symbol}

        if orderId:
            if startTime or endTime or fromId or limit:
                print("OrderId can only be used together with symbol !")
                exit(-1)
            else:
                payload["orderId"] = orderId
        
        if startTime:
            payload["startTime"] = startTime
        
        if endTime:
            payload["endTime"] = endTime
        
        if fromId:
            payload["fromId"] = fromId
        
        if limit:
            payload["limit"] = limit if limit < 1000 else 1000
        
        if recvWindow:
            recvWindow_max = 60000
            payload["recvWindow"] = recvWindow if recvWindow < recvWindow_max else recvWindow_max
        
        http_method = "get"
        complete_endpoint = self.api_endpoint + "/api/v3/myTrades"

        response = self._send_signed_request(http_method=http_method, complete_endpoint=complete_endpoint, payload=payload)

        return response
    
    def query_current_order_count_usage(self, recvWindow: int=None):

        payload = {}

        if recvWindow:
            recvWindow_max = 60000
            payload["recvWindow"] = recvWindow if recvWindow < recvWindow_max else recvWindow_max
        
        http_method = "get"
        complete_endpoint = self.api_endpoint + "/api/v3/rateLimit/order"

        response = self._send_signed_request(http_method=http_method, complete_endpoint=complete_endpoint, payload=payload)

        return response

    def create_spot_listen_key(self):

        payload = {}

        http_method = "post"
        complete_endpoint = self.api_endpoint + "/api/v3/userDataStream"

        response = self._send_request(http_method=http_method, complete_endpoint=complete_endpoint, payload=payload)

        return response
    
    def keep_alive_spot_listen_key(self, listenKey: str):

        payload = {"listenKey": listenKey}

        http_method = "put"
        complete_endpoint = self.api_endpoint + "/api/v3/userDataStream"

        response = self._send_request(http_method=http_method, complete_endpoint=complete_endpoint, payload=payload)

        return response

    def close_spot_listen_key(self, listenKey: str):
        
        payload = {"listenKey": listenKey}

        http_method = "delete"
        complete_endpoint = self.api_endpoint + "/api/v3/userDataStream"

        response = self._send_request(http_method=http_method, complete_endpoint=complete_endpoint, payload=payload)

        return response

    ########## TRADE ############

    def _new_order(self, symbol: str=None, side: OrderSide=None, type: OrderType=None, timeInForce: TimeInForce=None,
                   quantity: float=None, quoteOrderQty: float=None, price: float=None, newClientOrderId: str=None, 
                   stopPrice: float=None, icebergQty: float=None, newOrderRespType: OrderResponseType=None, recvWindow: int=None) -> requests.Response:
        
        if not symbol in self.symbol_information:
            self._get_symbol_information(symbol)
        
        baseAssetPrecision = self.symbol_information[symbol]["baseAssetPrecision"]
        quoteAssetPrecision = self.symbol_information[symbol]["quoteAssetPrecision"]
        stepSize = self.symbol_information[symbol]["stepSize"]
        tickSize = self.symbol_information[symbol]["tickSize"]
        
        quantity = round(quantity, int(-math.log10(stepSize)))
        quantity_str = '{:.{prec}f}'.format(quantity, prec=baseAssetPrecision)
        
        price_str = ""
        if price:
            price = round(price, int(-math.log10(tickSize)))
            price_str = '{:.{prec}f}'.format(price, prec=quoteAssetPrecision)

        stopPrice_str = ""
        if stopPrice:
            stopPrice = round(stopPrice, int(-math.log10(tickSize)))
            stopPrice_str = '{:.{prec}f}'.format(stopPrice, prec=quoteAssetPrecision)

        if not symbol or not side or not type:
            print("Symbol, Order Side, and Order Type are mandatory !")
            exit(-1)
        
        payload = {"symbol": symbol, "side": side.value, "type": type.value}

        if type == OrderType.LIMIT:
            if not timeInForce or not quantity or not price:
                print("Time in force, Quantity, and Price are mandatory for Limit orders !")
                exit(-1)
            
            if quantity <= 0 or price <= 0:
                print("Quantity and Price need to be > 0 ! ")
                exit(-1)


            payload["timeInForce"] = timeInForce.value

            # Quantity needs to pass the lot size test of (quantity - minQty) % stepSize == 0, we enforce this by having quantity % stepSize == 0 since minQty = stepSize in all observed cases
            payload["quantity"] = quantity_str
            payload["price"] = price_str

        elif type == OrderType.MARKET:
            if not quantity and not quoteOrderQty:
                print("Quantity or Quote Order Quantity are mandatory for Market orders !")
                exit(-1)
        
            if quantity and quoteOrderQty:
                print("Only one of Quantity and Quote Order Quantity can be passed !")
                exit(-1)
            
            if quantity:
                if quantity <= 0:
                    print("Quantity needs to be > 0 !")
                    exit(-1)
                
                payload["quantity"] = quantity_str
            
            if quoteOrderQty:
                if quoteOrderQty <= 0:
                    print("Quote Order Quantity needs to be > 0 !")
                    exit(-1)
                
                payload["quoteOrderQty"] = quoteOrderQty
        elif type == OrderType.STOP_LOSS:
            if not quantity or not stopPrice:
                print("Quantity and Stop Price are mandatory for Stop Loss orders !")
                exit(-1)
            
            if quantity <= 0 or stopPrice <= 0:
                print("Quantity and Stop Price need to be > 0 !")
                exit(-1)

            payload["quantity"] = quantity_str
            payload["stopPrice"] = stopPrice_str
        elif type == OrderType.STOP_LOSS_LIMIT:
            if not timeInForce or not quantity or not price or not stopPrice:
                print("Time in force, Quantity, Price, and Stop Price are mandatory for Stop Loss Limit orders !")
                exit(-1)
            
            if quantity <= 0 or price <= 0 or stopPrice <= 0:
                print("Quantity, Price, and Stop Price need to be > 0 !")
                exit(-1)
            
            payload["timeInForce"] = timeInForce.value
            payload["quantity"] = quantity_str
            payload["stopPrice"] = stopPrice_str
        elif type == OrderType.TAKE_PROFIT:
            if not quantity or not stopPrice:
                print("Quantity and Stop Price are mandatory for Take Profit orders !")
                exit(-1)
            
            if quantity <= 0 or stopPrice <= 0:
                print("Quantity and Stop Price need to be > 0 !")
                exit(-1)

            payload["quantity"] = quantity_str
            payload["stopPrice"] = stopPrice_str
        elif type == OrderType.TAKE_PROFIT_LIMIT:
            if not timeInForce or not quantity or not price or not stopPrice:
                print("Time in force, Quantity, Price, and Stop Price are mandatory for Take Profit Limit orders !")
                exit(-1)
            
            if quantity <= 0 or price <= 0 or stopPrice <= 0:
                print("Quantity, Price, and Stop Price need to be > 0 !")
                exit(-1)
            
            payload["timeInForce"] = timeInForce.value
            payload["quantity"] = quantity_str
            payload["price"] = price_str
            payload["stopPrice"] = stopPrice_str
        elif type == OrderType.LIMIT_MAKER:
            if not quantity or not price:
                print("Quantity and Price are mandatory for Limit Maker orders !")
                exit(-1)
            
            if quantity <= 0 or price <= 0:
                print("Quantity and Price need to be > 0 !")
                exit(-1)
            
            payload["quantity"] = quantity_str
            payload["price"] = price_str
        else:
            print(f"Unknown order type: {type} !")
            exit(-1)
        
        if icebergQty:
            if timeInForce != TimeInForce.GTC:
                print("Time in force has to be set to GTC for orders with a set Iceberg Quantity")
                exit(-1)
            
            if icebergQty <= 0:
                print("Iceberg Quantity needs to be > 0 !")
                exit(-1)
            
            payload["icebergQty"] = icebergQty
        
        if recvWindow:
            if recvWindow > 60000:
                print("Receive Window cannot be greater than 60000 !")
                exit(-1)
            elif recvWindow <= 0:
                print("Receive Window needs to be > 0 !")
                exit(-1)

            payload["recvWindow"] = recvWindow


        if newClientOrderId:
            payload["newClientOrderId"] = newClientOrderId
        
        if newOrderRespType:
            payload["newOrderRespType"] = newOrderRespType.value


        http_method = "post"
        complete_endpoint = self.api_endpoint + "/api/v3/order"

        response = self._send_signed_request(http_method=http_method, complete_endpoint=complete_endpoint, payload=payload)

        return response

    def buy_market_order(self, symbol: str, quantity: float=None, quoteOrderQty: float=None, recvWindow: int=5000, newOrderRespType: OrderResponseType=OrderResponseType.FULL) -> requests.Response:
        # Example symbol: BTCUSDT
        # quoteOrderQty on BUY side will buy as much BTC as quoteOrderQty USDT will allow us

        return self._new_order(symbol=symbol, side=OrderSide.BUY, type=OrderType.MARKET, quantity=quantity, quoteOrderQty=quoteOrderQty, recvWindow=recvWindow, newOrderRespType=newOrderRespType)
    

    def sell_market_order(self, symbol: str, quantity: float=None, quoteOrderQty: float=None, recvWindow: int=5000, newOrderRespType: OrderResponseType=OrderResponseType.FULL) -> requests.Response:
        # Example symbol: BTCUSDT
        # quoteOrderQty on SELL side will sell as much BTC needed to obtain the quoteOrderQty of USDT

        return self._new_order(symbol=symbol, side=OrderSide.SELL, type=OrderType.MARKET, quantity=quantity, quoteOrderQty=quoteOrderQty, recvWindow=recvWindow, newOrderRespType=newOrderRespType)


    def buy_limit_order(self, symbol: str, quantity: float, price: float, timeInForce: TimeInForce=TimeInForce.GTC, recvWindow: int=5000, newOrderRespType: OrderResponseType=OrderResponseType.FULL) -> requests.Response:
        # Example symbol: LTCBTC
        # Will buy quantity LTC for the price BTC per LTC or lower

        return self._new_order(symbol=symbol, side=OrderSide.BUY, type=OrderType.LIMIT, quantity=quantity, price=price, timeInForce=timeInForce, recvWindow=recvWindow, newOrderRespType=newOrderRespType)


    def sell_limit_order(self, symbol: str, quantity: float, price: float, timeInForce: TimeInForce=TimeInForce.GTC, recvWindow: int=5000, newOrderRespType: OrderResponseType=OrderResponseType.FULL) -> requests.Response:
        # Example symbol: LTCBTC
        # Will sell quantity LTC for the price BTC per LTC or higher

        return self._new_order(symbol=symbol, side=OrderSide.SELL, type=OrderType.LIMIT, quantity=quantity, price=price, timeInForce=timeInForce, recvWindow=recvWindow, newOrderRespType=newOrderRespType)
    
    def cancel_order(self, symbol:str, orderId: int=None, origClientOrderId: str=None, newClientOrderId: str=None, recvWindow:int=5000):
        if not symbol:
            print("Symbol is mandatory to cancel orders !")
            exit(-1)

        payload = {"symbol": symbol, "recvWindow": recvWindow}

        if not orderId and not origClientOrderId:
            print("OrderId or origClientOrderId are mandatory to cancel orders !")
            exit(-1)
        elif orderId:
            payload["orderId"] = orderId
        elif origClientOrderId:
            payload["origClientOrderId"] = origClientOrderId
        
        if newClientOrderId:
            payload["newClientOrderId"] = newClientOrderId
        
        
        http_method = "delete"
        complete_endpoint = self.api_endpoint + "/api/v3/order"

        response = self._send_signed_request(http_method=http_method, complete_endpoint=complete_endpoint, payload=payload)

        return response
    
    def cancel_all_open_orders_on_symbol(self, symbol: str, recvWindow: int=5000):

        payload = {"symbol": symbol, "recvWindow": recvWindow}

        http_method = "delete"
        complete_endpoint = self.api_endpoint + "/api/v3/openOrders"

        response = self._send_signed_request(http_method=http_method, complete_endpoint=complete_endpoint, payload=payload)

        return response
    
    def new_oco_order(self, symbol: str, side: OrderSide, quantity: float, price: float, stopPrice: float, stopLimitPrice: float,
                      listClientOrderId: str=None, limitClientOrderId: str=None, limitIcebergQty: float=None, trailingDelta: float=None,
                      stopClientOrderId: str=None, stopIcebergQty: float=None, stopLimitTimeInForce: TimeInForce=None,
                      newOrderRespType: OrderResponseType=None, recvWindow: int=5000):
        # price is the price of the Limit order i.e. the take profit price
        # stopPrice is the price at which the Stop-Limit order will be triggered
        # stopLimitPrice is the actual price of the Stop-Limit order when triggered

        if stopLimitPrice and not stopLimitTimeInForce:
            print("If stopLimitPrice is passed then stopLimitTimeInForce also has to be passed !")
            exit(-1)

        payload = {"symbol": symbol, "side": side.value, "recvWindow": recvWindow}

        if not symbol in self.symbol_information:
            self._get_symbol_information(symbol)

        baseAssetPrecision = self.symbol_information[symbol]["baseAssetPrecision"]
        quoteAssetPrecision = self.symbol_information[symbol]["quoteAssetPrecision"]
        stepSize = self.symbol_information[symbol]["stepSize"]
        tickSize = self.symbol_information[symbol]["tickSize"]

        quantity = round(quantity, int(-math.log10(stepSize)))
        quantity_str = '{:.{prec}f}'.format(quantity, prec=baseAssetPrecision)

        price = round(price, int(-math.log10(tickSize)))
        price_str = '{:.{prec}f}'.format(price, prec=quoteAssetPrecision)

        stopPrice = round(stopPrice, int(-math.log10(tickSize)))
        stopPrice_str = '{:.{prec}f}'.format(stopPrice, prec=quoteAssetPrecision)

        stopLimitPrice = round(stopLimitPrice, int(-math.log10(tickSize)))
        stopLimitPrice_str = '{:.{prec}f}'.format(stopLimitPrice, prec=quoteAssetPrecision)


        # Quantity needs to pass the lot size test of (quantity - minQty) % stepSize == 0, we enforce this by having quantity % stepSize == 0 since minQty = stepSize in all observed cases
        payload["quantity"] = quantity_str

        if side == OrderSide.SELL:
            if price > stopPrice > stopLimitPrice:
                payload["price"] = price_str
                payload["stopPrice"] = stopPrice_str
                payload["stopLimitPrice"] = stopLimitPrice_str
            else:
                print(f"OCO Sell order needs to have Price = {price} > stopPrice = {stopPrice} > stopLimitPrice = {stopLimitPrice}")
                exit(-1)
        elif side == OrderSide.BUY:
            if price < stopPrice < stopLimitPrice:
                payload["price"] = price_str
                payload["stopPrice"] = stopPrice_str
                payload["stopLimitPrice"] = stopLimitPrice_str
            else:
                print(f"OCO Buy order needs to have Price = {price} < stopPrice = {stopPrice} < stopLimitPrice = {stopLimitPrice}")
                exit(-1)

        if listClientOrderId:
            payload["listClientOrderId"] = listClientOrderId
        
        if limitClientOrderId:
            payload["limitClientOrderId"] = limitClientOrderId
        
        if limitIcebergQty:
            payload["limitIcebergQty"] = limitIcebergQty
        
        if trailingDelta:
            payload["trailingDelta"] = trailingDelta
        
        if stopClientOrderId:
            payload["stopClientOrderId"] = stopClientOrderId
        
        if stopIcebergQty:
            payload["stopIcebergQty"] = stopIcebergQty
        
        if stopLimitTimeInForce:
            payload["stopLimitTimeInForce"] = stopLimitTimeInForce.value
        
        if newOrderRespType:
            payload["newOrderRespType"] = newOrderRespType.value

        http_method = "post"
        complete_endpoint = self.api_endpoint + "/api/v3/order/oco"

        response = self._send_signed_request(http_method=http_method, complete_endpoint=complete_endpoint, payload=payload)

        return response
    
    def cancel_oco_order(self, symbol: str, orderListId: int=None, listClientOrderId: str=None, newClientOrderId: str=None, recvWindow: int=None):

        payload = {"synbol": symbol}

        if not orderListId and not listClientOrderId:
            print("OrderListId or ListClientOrderId has to be provided !")
            exit(-1)
        
        if orderListId:
            payload["orderListId"] = orderListId
        
        if listClientOrderId:
            payload["listClientOrderId"] = listClientOrderId
        
        if newClientOrderId:
            payload["newClientOrderId"] = newClientOrderId
        
        if recvWindow and recvWindow < 60000:
            payload["recvWindow"] = recvWindow
        
        http_method = "delete"
        complete_endpoint = self.api_endpoint + "/api/v3/orderList"
        
        response = self._send_signed_request(http_method=http_method, complete_endpoint=complete_endpoint, payload=payload)

        return response

class UserDataStreamType(Enum):
    OUTBOUND_ACCOUNT_POSITION = "outboundAccountPosition"
    BALANCE_UPDATE = "balanceUpdate"
    EXECUTION_REPORT = "executionReport"
    LIST_STATUS = "listStatus"

def read_outbound_account_position(tick_data):

    outbound_account_position = {"event_type": UserDataStreamType.OUTBOUND_ACCOUNT_POSITION.value,
                                 "event_time": int(tick_data["E"]),
                                 "time_last_account_update": int(tick_data["u"]),
                                 "balances": []}

    for balance in tick_data["B"]:
        outbound_account_position["balances"].append({"asset": balance["a"],
                                                      "free": float(balance["f"]),
                                                      "locked": float(balance["l"])})

    return outbound_account_position

def read_balance_update(tick_data):
    
    balance_update = {"event_type": UserDataStreamType.BALANCE_UPDATE.value,
                      "event_time": int(tick_data["E"]),
                      "asset": tick_data["a"],
                      "balance_delta": float(tick_data["d"]),
                      "clear_time": int(tick_data["T"])}
    
    return balance_update

def read_execution_report(tick_data):

    execution_report = {"event_type": UserDataStreamType.EXECUTION_REPORT.value,
                        "event_time": int(tick_data["E"]),
                        "symbol": tick_data["s"],
                        "client_order_id": tick_data["c"],
                        "side": tick_data["S"],
                        "order_type": tick_data["o"],
                        "time_in_force": tick_data["f"],
                        "order_quantity": float(tick_data["q"]),
                        "order_price": float(tick_data["p"]),
                        "stop_price": float(tick_data["P"]),
                        "iceberg_quantity": float(tick_data["F"]),
                        "order_list_id": tick_data["g"],
                        "original_client_order_id": tick_data["C"],
                        "current_execution_type": tick_data["x"],
                        "current_order_status": tick_data["X"],
                        "order_reject_reason": tick_data["r"],
                        "order_id": tick_data["i"],
                        "last_executed_quantity": float(tick_data["l"]),
                        "cumulative_filled_quantity": float(tick_data["z"]),
                        "last_executed_price": float(tick_data["L"]),
                        "commission_amount": float(tick_data["n"]),
                        "commission_asset": tick_data["N"],
                        "transaction_time": int(tick_data["T"]),
                        "trade_id": tick_data["t"],
                        "is_order_on_book": tick_data["w"],
                        "is_this_maker_side": tick_data["m"],
                        "order_creation_time": int(tick_data["O"]),
                        "cumulative_quote_asset_transacted_quantity": float(tick_data["Z"]),
                        "last_quote_asset_transacted_quantity": float(tick_data["Y"]), # i.e. lastPrice * lastQty
                        "quote_order_quantity": float(tick_data["Q"])}
    
    if "d" in tick_data:
        # Trailing Delta; This is only visible if the order was a trailing stop order
        execution_report["trailing_delta"] = tick_data["d"]
    
    return execution_report

def read_list_status(tick_data):

    list_status = {"event_type": UserDataStreamType.LIST_STATUS.value,
                   "event_time": int(tick_data["E"]),
                   "symbol": tick_data["s"],
                   "order_list_id": tick_data["g"],
                   "contingency_type": tick_data["c"],
                   "list_status_type": tick_data["l"],
                   "list_order_status": tick_data["L"],
                   "list_reject_reason": tick_data["r"],
                   "list_client_order_id": tick_data["C"],
                   "transaction_time": int(tick_data["T"]),
                   "object_array": []}
    
    for object in tick_data["O"]:
        list_status["object_array"].append({"symbol": object["s"],
                                            "order_id": object["i"],
                                            "client_order_id": object["c"]})
    
    return list_status

def read_binance_user_data_stream(tick_data):

    user_data_stream_analyzers = {UserDataStreamType.OUTBOUND_ACCOUNT_POSITION.value: read_outbound_account_position,
                                  UserDataStreamType.BALANCE_UPDATE.value: read_balance_update,
                                  UserDataStreamType.EXECUTION_REPORT.value: read_execution_report,
                                  UserDataStreamType.LIST_STATUS.value: read_list_status}
    
    return user_data_stream_analyzers[tick_data["e"]](tick_data=tick_data)


def outbound_account_position_to_human_text(tick_data):

    read_tick_data = read_outbound_account_position(tick_data=tick_data)

    readable_date = datetime.fromtimestamp(read_tick_data["event_time"] / 1000).strftime("%Y-%m-%d %H:%M:%S")
    
    human_text = f"Event: Outbound Account Position.\nDate: {readable_date}.\nAccount balance changed ! Following assets changed: \n"

    for balance in read_tick_data["balances"]:
        human_text += f"{balance['asset']}: Free = {balance['free']}, Locked = {balance['locked']}\n"
    
    return human_text
    

def balance_update_to_human_text(tick_data):

    read_tick_data = read_binance_user_data_stream(tick_data=tick_data)

    readable_date = datetime.fromtimestamp(read_tick_data["event_time"] / 1000).strftime("%Y-%m-%d %H:%M:%S")

    human_text = f"Event: Balance Update.\nDate: {readable_date}.\n"
    human_text += f"A change in {read_tick_data['asset']} of {read_tick_data['balance_delta']}.\n"
    
    readable_clear_time = datetime.fromtimestamp(read_tick_data["clear_time"] / 1000).strftime("%Y-%m-%d %H:%M:%S")
    human_text += f"Cleared on: {readable_clear_time}."

    return human_text

def execution_report_to_human_text(tick_data):

    read_tick_data = read_execution_report(tick_data=tick_data)

    readable_date = datetime.fromtimestamp(read_tick_data["event_time"] / 1000).strftime("%Y-%m-%d %H:%M:%S")

    human_text = f"Event: Execution Report.\nDate: {readable_date}.\n"
    readable_order_creation_time = datetime.fromtimestamp(read_tick_data["order_creation_time"] / 1000).strftime("%Y-%m-%d %H:%M:%S")

    symbol = read_tick_data["symbol"]
    human_text += f"Order for {symbol}. Created on: {readable_order_creation_time}.\n"
    human_text += f"Client Order ID: {read_tick_data['client_order_id']}.\nOrder ID: {read_tick_data['order_id']}.\n"
    human_text += f"Payed {read_tick_data['commission_amount']} of {read_tick_data['commission_asset']} as commission.\n"
    human_text += f"Time in force: {read_tick_data['time_in_force']}.\n"

    execution_order_type_strings = {ExecutionOrderType.NEW.value: "The order is NEW and has been accepted by the engine.\n",
                                    ExecutionOrderType.CANCELED.value: "The order has been CANCELED by the user.\n",
                                    ExecutionOrderType.TRADE.value: "Part or all of the TRADE order's quantity has been filled.\n",
                                    ExecutionOrderType.EXPIRED.value: "The order EXPIRED due to the order type's rules or by the exchange.\n"}
    execution_order_type = read_tick_data["current_execution_type"]
    human_text += execution_order_type_strings[execution_order_type]

    order_reject_reason = read_tick_data['order_reject_reason']
    if order_reject_reason != "NONE":
        human_text += f"Order got rejected with reason: {order_reject_reason}"

    order_side = read_tick_data["side"]
    order_type = read_tick_data["order_type"]
    if execution_order_type == ExecutionOrderType.NEW.value:
        human_text += f"Made a NEW {order_type} to {order_side} {read_tick_data['order_quantity']} {symbol} at a price of {read_tick_data['order_price']}.\n"
        human_text += f"A total of {read_tick_data['order_quantity'] * read_tick_data['order_price']}.\n"
        if order_type == OrderType.STOP_LOSS_LIMIT.value:
            human_text += f"This order is triggered at a stop price of {read_tick_data['stop_price']}.\n"
    elif execution_order_type == ExecutionOrderType.TRADE.value:
        human_text += f"The {order_type} {order_side} order filled a cumulative quantity of {read_tick_data['cumulative_filled_quantity']} of the ordered {read_tick_data['order_quantity']}.\n"
        human_text += f"Cumulative quote asset transacted quantity: {read_tick_data['cumulative_quote_asset_transacted_quantity']}"
    elif execution_order_type == ExecutionOrderType.EXPIRED.value:
        human_text += f"The {order_type} to {order_side} {read_tick_data['order_quantity']} {symbol} at a price of {read_tick_data['order_price']} expired/failed.\n"
    elif execution_order_type == ExecutionOrderType.CANCELED.value:
        human_text += f"The order that was canceled was a {order_side} {order_type} order for {read_tick_data['order_quantity']} at a price of {read_tick_data['order_price']}."

    return human_text

def list_status_to_human_text(tick_data):

    read_tick_data = read_binance_user_data_stream(tick_data=tick_data)

    readable_date = datetime.fromtimestamp(read_tick_data["event_time"] / 1000).strftime("%Y-%m-%d %H:%M:%S")

    human_text = f"Event: List Status.\nDate: {readable_date}.\n"
    if read_tick_data["list_order_status"] == OCOOrderStatus.ALL_DONE.value:
        human_text += "OCO ALL DONE.\n"
    elif read_tick_data["list_order_status"] == OCOOrderStatus.EXECUTING.value:
        human_text += "OCO Order Created.\n"
    human_text += f"Order for {read_tick_data['symbol']}.\n"
    
    readable_transaction_time = datetime.fromtimestamp(read_tick_data["transaction_time"] / 1000).strftime("%Y-%m-%d %H:%M:%S")
    human_text += f"Transaction time: {readable_transaction_time}\n"

    human_text += f"Order List ID: {read_tick_data['order_list_id']}.\n"
    human_text += f"List Client Order ID: {read_tick_data['list_client_order_id']}.\n"

    if read_tick_data["list_reject_reason"] != "NONE":
        human_text += f"REJECTED with reason {read_tick_data['list_reject_reason']}.\n"
    else:
        human_text += f"Orders contained within this list:\n"

        for order in read_tick_data["object_array"]:
            human_text += f"Order for {order['symbol']} with order ID: {order['order_id']} and Client Order ID: {order['client_order_id']}.\n"

    return human_text

def user_data_stream_to_human_text(tick_data):

    user_data_to_human_converters = {UserDataStreamType.OUTBOUND_ACCOUNT_POSITION.value: outbound_account_position_to_human_text,
                                     UserDataStreamType.BALANCE_UPDATE.value: balance_update_to_human_text,
                                     UserDataStreamType.EXECUTION_REPORT.value: execution_report_to_human_text,
                                     UserDataStreamType.LIST_STATUS.value: list_status_to_human_text}

    return user_data_to_human_converters[tick_data["e"]](tick_data=tick_data)


if __name__ == "__main__":
    test_endpoint = "https://testnet.binance.vision"
    binance = Binance(api_endpoint=test_endpoint, api_key=secrets.binance_test_apikey, api_secret=secrets.binance_test_secret)

    print(binance.get_account_info())
    balances = binance.get_account_info()["balances"]
    current_usdt_balance = 0
    for balance in balances:
        if balance["asset"] == "USDT":
            current_usdt_balance = float(balance["free"])
            break
    
    print(f"USDT Balance = {current_usdt_balance}")

    print(binance.current_open_orders("ETHUSDT"))

    print(binance.query_order("ETHUSDT", 3083786))