import numpy as np
from Positions.HistoricalPositions import LongPosition, ShortPosition

class PositionManagerHistorical:

    def __init__(self, wallet, n_position_limit=-1):
        
        self.n_position_limit = n_position_limit
        self.limit_open_positions = False if self.n_position_limit < 0 else True
        self.open_long_positions = {}
        self.open_short_positions = {}
        self.wallet = wallet

        self.closed_long_positions = {}
        self.closed_short_positions = {}
        self.profitted_positions = {}
        self.loss_positions = {}
        self.win_rate = {}

        self.close_times = np.array([])
        self.wallet_after_close = np.array([])


    def try_open_long_position(self, price_data, capital_fraction, profit_target_price, stop_loss_price):
        wallet_before_open = float(self.wallet)
        buy_value = capital_fraction * self.wallet
        buy_amount = buy_value / price_data["close_price"]
        wallet_after_open = wallet_before_open - buy_value

        new_position = LongPosition(wallet_before_open=wallet_before_open,
                                wallet_after_open=wallet_after_open,
                                symbol=price_data["symbol"], 
                                open_price=price_data["close_price"], 
                                open_time=price_data["close_time"],
                                profit_target_price=profit_target_price,
                                stop_loss_price=stop_loss_price,
                                buy_value=buy_value,
                                buy_amount=buy_amount)
        
        if not price_data["symbol"] in self.open_long_positions:
            self.open_long_positions[price_data["symbol"]] = []

        if self.limit_open_positions:
            if price_data["symbol"] in self.open_long_positions:
                if len(self.open_long_positions[price_data["symbol"]]) < self.n_position_limit:
                    self.open_long_positions[price_data["symbol"]].append(new_position)
                    self.wallet -= buy_value
        else:
            self.open_long_positions[price_data["symbol"]].append(new_position)
            self.wallet -= buy_value
        
        return new_position



    def try_close_long_positions(self, price_data):
        symbol = price_data["symbol"]
        if symbol in self.open_long_positions:

            for position in self.open_long_positions[symbol]:

                if price_data["close_price"] >= position.profit_target_price or (position.stop_loss_price > 0 and price_data["close_price"] <= position.stop_loss_price):
                    wallet_before_close = float(self.wallet)
                    sell_value = position.buy_amount * price_data["close_price"] 
                    self.wallet += sell_value

                    position.close(close_price=price_data["close_price"], 
                                    close_time=price_data["close_time"],
                                    wallet_before_close=wallet_before_close,
                                    wallet_after_close=self.wallet,
                                    sell_value=sell_value)
                    
                    if not symbol in self.closed_long_positions:
                        self.closed_long_positions[symbol] = []
                        self.profitted_positions[symbol] = 0
                        self.loss_positions[symbol] = 0
                        self.win_rate[symbol] = 0
                    
                    self.closed_long_positions[price_data["symbol"]].append(position)

                    self.close_times = np.append(self.close_times, price_data["close_time"])
                    self.wallet_after_close = np.append(self.wallet_after_close, self.wallet)

                    if position.net_value > 0:
                        self.profitted_positions[symbol] += 1
                    elif position.net_value < 0:
                        self.loss_positions[symbol] += 1
                    
                    self.win_rate[symbol] = self.profitted_positions[symbol] / (self.profitted_positions[symbol] + self.loss_positions[symbol])


            if symbol in self.closed_long_positions:
                for position in self.closed_long_positions[symbol]:
                    try:
                        self.open_long_positions[symbol].remove(position)
                    except:
                        pass
    
    def try_open_short_position(self, price_data, capital_fraction, profit_target_price, stop_loss_price):
        wallet_before_open = float(self.wallet)
        sell_value = capital_fraction * self.wallet
        sell_amount = sell_value / price_data["close_price"]
        wallet_after_open = wallet_before_open + sell_value

        new_position = ShortPosition(wallet_before_open=wallet_before_open,
                                wallet_after_open=wallet_after_open,
                                symbol=price_data["symbol"], 
                                open_price=price_data["close_price"], 
                                open_time=price_data["close_time"],
                                profit_target_price=profit_target_price,
                                stop_loss_price=stop_loss_price, 
                                sell_value=sell_value,
                                buy_amount=sell_amount)


        if not price_data["symbol"] in self.open_short_positions:
            self.open_short_positions[price_data["symbol"]] = []

        if self.limit_open_positions:
            if price_data["symbol"] in self.open_short_positions:
                if len(self.open_short_positions[price_data["symbol"]]) < self.n_position_limit:
                    self.open_short_positions[price_data["symbol"]].append(new_position)
                    self.wallet += sell_value
        else:
            self.open_short_positions[price_data["symbol"]].append(new_position)
            self.wallet += sell_value
        
        return new_position


    def try_close_short_positions(self, price_data):
        symbol = price_data["symbol"]
        if symbol in self.open_short_positions:

            for position in self.open_short_positions[symbol]:

                buy_value = position.buy_amount * price_data["close_price"]
                if (price_data["close_price"] <= position.profit_target_price or price_data["close_price"] >= position.stop_loss_price) and buy_value <= self.wallet:

                    wallet_before_close = float(self.wallet)
                    self.wallet -= buy_value

                    position.close(close_price=price_data["close_price"], 
                                    close_time=price_data["close_time"],
                                    wallet_before_close=wallet_before_close,
                                    wallet_after_close=self.wallet,
                                    kwargs={"buy_value": buy_value})

                    if not symbol in self.closed_short_positions:
                        self.closed_short_positions[symbol] = []
                    
                    self.closed_short_positions[price_data["symbol"]].append(position)

                    self.close_times = np.append(self.close_times, price_data["close_time"])
                    self.wallet_after_close = np.append(self.wallet_after_close, self.wallet)

                    print("Closing: ", position)

            if symbol in self.closed_short_positions:
                for position in self.closed_short_positions[symbol]:
                    try:
                        self.open_short_positions[symbol].remove(position)
                    except:
                        pass
    
    def try_close_positions(self, price_data):
        self.try_close_long_positions(price_data=price_data)
        self.try_close_short_positions(price_data=price_data)