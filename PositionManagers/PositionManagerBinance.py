import logging

import ExchangeAPIs.Binance as Binance
from Positions.Positions import OpenLongPosition, ClosedLongPosition
from Positions.DBPositionModels import DBClosedLongPosition, DBOpenLongPosition
from Utils.telegrambot import send_telegram_message
import requests

from sqlalchemy import select
from sqlalchemy.orm import Session
from sqlalchemy.engine import Engine

class PositionManagerBinance:

    def __init__(self, binance: Binance, wallet_asset: str, 
                 n_position_limit: int=-1, db_engine: Engine=None,
                 enable_telegram: bool=False, factor_min_notional: float=1.02):
        
        # Limit the number of positions per symbol we can have open
        self.n_position_limit = n_position_limit
        self.limit_open_positions = False if self.n_position_limit < 0 else True
        self.n_open_positions = 0
        self.open_long_positions = {}
        self.partner_order_id = {}

        self.min_notional = {}
        self.base_asset_precision = {}
        self.quote_asset_precision = {}
        self.max_num_algo_orders = {}
        self.factor_min_notional = factor_min_notional

        self.binance = binance
        self.wallet_asset = wallet_asset

        self.enable_telegram = enable_telegram
        self.db_engine = db_engine

        self._initialize_open_positions()


    def _get_symbol_information(self, symbol: str):
        baseAssetPrecision = int(self.binance.get_exchange_info(symbol)["symbols"][0]["baseAssetPrecision"])
        quoteAssetPrecision = int(self.binance.get_exchange_info(symbol)["symbols"][0]["quoteAssetPrecision"])
        symbol_info = self.binance.get_symbol_filters(symbol)
        min_notional = symbol_info[Binance.SymbolFilter.MIN_NOTIONAL.value]
        min_notional = float(min_notional["minNotional"])

        max_num_algo_orders = symbol_info[Binance.SymbolFilter.MAX_NUM_ALGO_ORDERS.value]
        max_num_algo_orders = int(max_num_algo_orders["maxNumAlgoOrders"])

        self.min_notional[symbol] = min_notional
        self.base_asset_precision[symbol] = baseAssetPrecision
        self.quote_asset_precision[symbol] = quoteAssetPrecision
        self.max_num_algo_orders[symbol] = max_num_algo_orders

    def get_base_asset_precision(self, symbol: str):
        if symbol in self.base_asset_precision:
            return self.base_asset_precision[symbol]
        else:
            self._get_symbol_information(symbol=symbol)
            return self.base_asset_precision[symbol]
    
    def get_quote_asset_precision(self, symbol: str):
        if symbol in self.quote_asset_precision:
            return self.quote_asset_precision[symbol]
        else:
            self._get_symbol_information(symbol=symbol)
            return self.quote_asset_precision[symbol]
    
    def get_min_notional(self, symbol: str):
        if symbol in self.min_notional:
            return self.min_notional[symbol]
        else:
            self._get_symbol_information(symbol=symbol)
            return self.min_notional[symbol]
    
    def get_max_num_algo_orders(self, symbol: str):
        if symbol in self.max_num_algo_orders:
            return self.max_num_algo_orders[symbol]
        else:
            self._get_symbol_information(symbol=symbol)
            return self.max_num_algo_orders[symbol]

    def get_wallet_value(self):

        balances = self.binance.get_account_info()["balances"]
        for balance in balances:
            if balance["asset"] == self.wallet_asset.upper():
                return float(balance["free"])

    def try_open_long_position(self, price_data, capital_fraction, profit_target_price, stop_loss_price, stop_loss_trigger_price):
        if not self.limit_open_positions or not (self.n_open_positions >= self.n_position_limit):
            buying_value = capital_fraction * self.get_wallet_value()
            quantity = buying_value / price_data["close_price"]
            
            symbol = price_data["symbol"]

            try:
                if self.n_open_positions < self.get_max_num_algo_orders(symbol) and buying_value >= self.factor_min_notional * self.get_min_notional(symbol):
                    # First try to buy the minnotional amount
                    min_notional_qty = self.get_min_notional(symbol) / price_data["close_price"]

                    response = self.binance.buy_limit_order(symbol=symbol, 
                                                            quantity=self.factor_min_notional * min_notional_qty, 
                                                            price=price_data["close_price"], 
                                                            timeInForce=Binance.TimeInForce.FOK)

                    if response["status"] == Binance.OrderStatus.FILLED.value:
                        bought_quantity = float(response["executedQty"])
                        buy_value = float(response["cummulativeQuoteQty"])
                        transaction_time = response["transactTime"]
                        price = float(response["price"])

                        # Try to buy the rest
                        response = self.binance.buy_limit_order(symbol=symbol, 
                                                                quantity=(quantity - bought_quantity), 
                                                                price=price_data["close_price"], 
                                                                timeInForce=Binance.TimeInForce.IOC)
                        if self.enable_telegram: 
                            send_telegram_message(f"Response: {response}")
                        
                        if  response["status"] == Binance.OrderStatus.FILLED.value or (response["status"] == Binance.OrderStatus.EXPIRED.value and response["fills"]):
                            bought_quantity += float(response["executedQty"])
                            buy_value += float(response["cummulativeQuoteQty"])

                            bought_quantity = round(bought_quantity, self.get_base_asset_precision(symbol))
                            buy_value = round(buy_value, self.get_quote_asset_precision(symbol))
                        
                        if self.enable_telegram:
                            send_telegram_message(f"We bought {bought_quantity} at the price of {price} !")
                        
                        response = self.binance.new_oco_order(symbol=symbol, 
                                                            side=Binance.OrderSide.SELL, 
                                                            quantity=bought_quantity, 
                                                            price=profit_target_price,
                                                            stopPrice=stop_loss_trigger_price, 
                                                            stopLimitPrice=stop_loss_price,
                                                            stopLimitTimeInForce=Binance.TimeInForce.GTC)
                        if self.enable_telegram:
                            send_telegram_message(f"Created OCO order response: {response}")
                        
                        stop_loss_order_id = -1
                        take_profit_order_id = -1

                        for order in response["orderReports"]:
                            if order["type"] == Binance.OrderType.STOP_LOSS_LIMIT.value:
                                stop_loss_order_id = order["orderId"]
                            elif order["type"] == Binance.OrderType.LIMIT_MAKER.value:
                                take_profit_order_id = order["orderId"]
                        oco_list_id = response["orderListId"]

                        new_position = OpenLongPosition(symbol=symbol,
                                                        take_profit_order_id=take_profit_order_id,
                                                        stop_loss_order_id=stop_loss_order_id,
                                                        oco_list_id=oco_list_id,
                                                        open_time=transaction_time,
                                                        buy_amount=bought_quantity,
                                                        buy_value=buy_value,
                                                        buy_price=price,
                                                        take_profit_price=profit_target_price,
                                                        stop_loss_price=stop_loss_price,
                                                        stop_loss_trigger_price=stop_loss_trigger_price)
                        
                        self._add_open_long_position(new_position)

                        print(f"Opened a long position:\n\r{new_position}")
                        logging.info(f"Opened a long position:\n\r{new_position}")

                        if self.enable_telegram:
                            send_telegram_message(f"Opened a long position:\n\r{new_position}")

                        if self.db_engine:
                            self._create_open_long_position_db(open_long_position=new_position)                  

            except requests.exceptions.RequestException as e:
                    print(e)
                    print(f"Request causing error: {e.request.url}")
                    print(f"Response after error: {e.response.json()}")

                    logging.error(f"Error: {e}")
                    logging.error(f"Request causing error: {e.request.url}")
                    logging.error(f"Response after error: {e.response.json()}")
                    
                    if self.enable_telegram:
                        send_telegram_message(f"Error: {e}.")
                        send_telegram_message(f"Request causing error: {e.request.url}")
                        send_telegram_message(f"Response after error: {e.response.json()}")

                    pass
    
    def _add_open_long_position(self, open_long_position: OpenLongPosition):
        self.open_long_positions[open_long_position.take_profit_order_id] = open_long_position
        self.open_long_positions[open_long_position.stop_loss_order_id] = open_long_position

        self.partner_order_id[open_long_position.take_profit_order_id] = open_long_position.stop_loss_order_id
        self.partner_order_id[open_long_position.stop_loss_order_id] = open_long_position.take_profit_order_id

        self.n_open_positions += 1
    
    def _remove_open_long_position(self, order_id: int):

        partner_order_id = self.partner_order_id[order_id]
        del self.open_long_positions[order_id]
        del self.open_long_positions[partner_order_id]

        del self.partner_order_id[order_id]
        del self.partner_order_id[partner_order_id]

        self.n_open_positions -= 1

    def fill_open_long_position(self, order_id, fill_amount, fill_value, fill_time):
        if order_id in self.open_long_positions:
            open_long_position = self.open_long_positions[order_id]
            if open_long_position.is_open:
                open_long_position.fill_position(fill_amount, fill_value)

                if self.db_engine:
                    self._fill_open_long_position_db(oco_list_id=open_long_position.oco_list_id, fill_amount=fill_amount, fill_value=fill_value)

                if not open_long_position.is_open:
                    closed_position = ClosedLongPosition(open_long_position=open_long_position, fill_time=fill_time)
                    
                    self._remove_open_long_position(order_id=order_id)

                    print(f"Closed a position:\n{closed_position}")
                    logging.info(f"Closed a position:\n{closed_position}")
                    
                    if self.enable_telegram:
                        send_telegram_message(f"Closed a long position:\n{closed_position}")

                    if self.db_engine:
                        self._close_open_long_position_db(closed_long_position=closed_position)
    
    
    def _create_open_long_position_db(self, open_long_position):
        with Session(self.db_engine) as session:
            q = session.query(DBOpenLongPosition).filter(DBOpenLongPosition.oco_list_id == open_long_position.oco_list_id)
            if not session.query(q.exists()).scalar():
                opened_position = DBOpenLongPosition(open_long_position=open_long_position)
                session.add(opened_position)
                session.commit()
    
    def _fill_open_long_position_db(self, oco_list_id: int, fill_amount: float, fill_value: float):

        with Session(self.db_engine) as session:
            stmt = select(DBOpenLongPosition).where(DBOpenLongPosition.oco_list_id == oco_list_id)
            db_open_long_position = session.execute(stmt).scalar_one()

            db_open_long_position.filled_amount = fill_amount
            db_open_long_position.sell_value = fill_value

            session.commit()
    
    def _close_open_long_position_db(self, closed_long_position):

        with Session(self.db_engine) as session:
            stmt = select(DBOpenLongPosition).where(DBOpenLongPosition.oco_list_id == closed_long_position.oco_list_id)

            db_open_long_position = session.execute(stmt).scalar_one()
            session.delete(db_open_long_position)

            db_closed_position = DBClosedLongPosition(closed_long_position=closed_long_position)
            session.add(db_closed_position)

            session.commit()
    
    def _initialize_open_positions(self):
        
        self._transform_open_orders_to_positions()

        if self.db_engine:
            
            for order_id, open_long_position in self.open_long_positions.items():
                self._create_open_long_position_db(open_long_position=open_long_position)

            self._refresh_positions_db()
            print(f"Added {self.n_open_positions} open long positions to the Position Manager !")
            logging.info(f"Added {self.n_open_positions} open long positions to the Position Manager !")
            for order_id, open_position in self.open_long_positions.items():
                print(f"Order ID: {order_id} and Open Position: {open_position}")
                logging.info(f"Order ID: {order_id} and Open Position: {open_position}")
    

    def _refresh_positions_db(self):

        with Session(self.db_engine) as session:

            result = session.execute(select(DBOpenLongPosition))

            for db_open_long_position in result.scalars().all():

                # Create and store the open long position
                open_long_position = OpenLongPosition(symbol=db_open_long_position.symbol,
                                                      take_profit_order_id=db_open_long_position.take_profit_order_id,
                                                      stop_loss_order_id=db_open_long_position.stop_loss_order_id,
                                                      oco_list_id=db_open_long_position.oco_list_id,
                                                      open_time=db_open_long_position.open_time_unix,
                                                      buy_amount=db_open_long_position.buy_amount,
                                                      buy_value=db_open_long_position.buy_value,
                                                      buy_price=db_open_long_position.buy_price,
                                                      take_profit_price=db_open_long_position.take_profit_price,
                                                      stop_loss_price=db_open_long_position.stop_loss_price,
                                                      stop_loss_trigger_price=db_open_long_position.stop_loss_trigger_price)

                self._add_open_long_position(open_long_position)
                
                take_profit_order = self.binance.query_order(db_open_long_position.symbol,
                                                            db_open_long_position.take_profit_order_id)
                
                stop_loss_order = self.binance.query_order(db_open_long_position.symbol,
                                                           db_open_long_position.stop_loss_order_id)

                orders = [take_profit_order, stop_loss_order]

                for order in orders:
                    if order["side"] == Binance.OrderSide.SELL.value:
                        if order["status"] in [Binance.OrderStatus.FILLED.value, Binance.OrderStatus.PARTIALLY_FILLED.value]:
                            self.fill_open_long_position(order_id=int(order["orderId"]), 
                                                        fill_amount=float(order["executedQty"]), 
                                                        fill_value=float(order["cummulativeQuoteQty"]),
                                                        fill_time=int(order["updateTime"]))
                            break
    
    def _transform_open_orders_to_positions(self):

        account_buy_lists = {}
        open_orders = self.binance.current_open_orders()

        open_orders_by_listid = {}

        for open_order in open_orders:
            order_list_id = open_order["orderListId"]
            if order_list_id in open_orders_by_listid:
                open_orders_by_listid[order_list_id].append(open_order)
            else:
                open_orders_by_listid[order_list_id] = [open_order]
        

        for list_id, open_orders in open_orders_by_listid.items():
            symbol = open_orders[0]["symbol"]

            if symbol not in account_buy_lists:
                trade_list = self.binance.get_account_trade_list(symbol=symbol)
                account_buy_lists[symbol] = [trade for trade in trade_list if trade["isBuyer"]]

            oco_list_id = list_id
            open_time = int(open_orders[0]["time"])
            buy_amount = float(open_orders[0]["origQty"])

            possible_buys = []
            for buy in account_buy_lists[symbol]:
                if open_time - 10 * 1000 <= buy["time"] <= open_time:
                    possible_buys.append(buy)
            
            buy_amount_buys = 0
            buy_value_buys = 0
            for buy in possible_buys:
                buy_amount_buys += float(buy["qty"])
                buy_value_buys += float(buy["quoteQty"])

            buy_amount_buys = round(buy_amount_buys, self.get_base_asset_precision(symbol))
            buy_value_buys = round(buy_value_buys, self.get_quote_asset_precision(symbol))
            
            if buy_amount_buys == buy_amount:
                buy_value = buy_value_buys
                buy_price = float(possible_buys[-1]["price"])

                take_profit_order_id = 0
                take_profit_price = 0.0
                stop_loss_order_id = 0
                stop_loss_price = 0.0
                stop_loss_trigger_price = 0.0

                for order in open_orders:
                    if order["type"] == Binance.OrderType.LIMIT_MAKER.value:
                        take_profit_order_id = order["orderId"]
                        take_profit_price = float(order["price"])
                    elif order["type"] == Binance.OrderType.STOP_LOSS_LIMIT.value:
                        stop_loss_order_id = order["orderId"]
                        stop_loss_price = float(order["price"])
                        stop_loss_trigger_price = float(order["stopPrice"])
                
                open_long_position = OpenLongPosition(symbol=symbol,
                                                      take_profit_order_id=take_profit_order_id,
                                                      stop_loss_order_id=stop_loss_order_id,
                                                      oco_list_id=oco_list_id,
                                                      open_time=open_time,
                                                      buy_amount=buy_amount,
                                                      buy_value=buy_value,
                                                      buy_price=buy_price,
                                                      take_profit_price=take_profit_price,
                                                      stop_loss_price=stop_loss_price,
                                                      stop_loss_trigger_price=stop_loss_trigger_price)

                print(f"Opened a previously opened position: {open_long_position}")
                logging.info(f"Opened a previously opened position: {open_long_position}")

                self._add_open_long_position(open_long_position=open_long_position)