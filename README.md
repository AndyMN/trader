# Trader
This repository contains all necessary components to build an automatic trading bot on (crypto)exchanges like Binance.
A class using `requests` is created to access the Binance REST API to get information and make trades.
Actions of the bot are logged to an output file and can be optionally send through a Telegram bot to the users phone.
Opened/closed positions are optionally stored within an sqlite3 database.

# Installation
Using the `requirements.txt` a virtual environment can be created contained all necessary libraries.

```bash
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

# secret_keys.py
One python file is not included within the repository and should be created by the user.
This file is the `secret_keys.py` file and should be placed in the root directory of the project.
The `secret_keys.py` file should contain the following variables:

```python
binance_main_apikey = "MAINNET_API_KEY"
binance_main_secret = "MAINNET_SECRET"

binance_test_apikey = "TESTNET_API_KEY"
binance_test_secret = "TESTNET_SECRET"

telegram_bot_token = "TELEGRAM_BOT_TOKEN"
telegram_chat_id = "TELEGRAM_CHAT_ID"
```

## Binance MainNet Keys
To trade on the Binance MainNet follow the instructions on: https://www.binance.com/en/support/faq/360002502072
This will deliver the `binance_main_apikey` and `binance_main_secret`.

## Binance TestNet Keys
To trade on the Binance Spot TestNet login on the following site using GitHub: https://testnet.binance.vision/
This will deliver the `binance_test_apikey` and `binance_test_secret`.

## Telegram bot tokens
To use the optional Telegram bot to notify the user of the bots actions you need to create a Telegram bot.
To create a Telegram bot follow the instructions on: https://core.telegram.org/bots#6-botfather
i.e. talk to the BotFather on Telegram and follow the steps of creating a new bot.
This will deliver the `telegram_bot_token`.

Next, we need to obtain the `telegram_chat_id`, this is the id of the chat conversation of the bot with yourself.
A bot cannot message to a user (through a phone number or username) unless a user talks to the bot first.
Send a message to your bot and then run the `telegrambot.py` script within the `Utils` folder.
This should print the `telegram_chat_id`.


# Applications

## mainHistorical.py
This application will apply an implemented trading strategy (`Signals/Long/<some_strategy>.py`) and test it on a dataset found in historical data.
Currently the dataset used is for the ETHEUR symbol on Binance for January - April 2022 on the 30m timeframe.
All bought positions and their take-profit and stop-loss prices are visualised in the created figures.
The ROI is printed in the console.

To get more historical data from Binance a helper bash script was created and can be found in `historical_data/download-klines.sh`.
To for example download all 2022 data for SOLUSDC on the 30m timeframe the following command can be used:

```bash
./historical_data/download-klines.sh -s SOLUSDC -y 2022 -i 30m
```

This will automagically download the requested data for each month and unzip the downloaded files and place them in the directory where the `mainHistorical.py` script will look for them.

## mainTestnet.py
This application can be used to test the application on the testnet of Binance without losing (or gaining) any real money. Each testnet account starts with plenty of assets to do sufficient trades. You should not test any trading strategy on the testnet since it does not represent the real world trading situation. This application trades on every closed kline, so no trading strategy is implemented.

It can be used to view data sent and received from Binance and to test the Telegram bot.

## main.py
This is the application which trades on the mainnet of Binance with the users actual funds. The structure is mostly the same as that of the `mainTestnet.py` application however a trading strategy in the form of a `Signals.Long.<trading_strategy>` is used to generate signals to buy assets.
