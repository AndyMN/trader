import Utils.indicator as indicator
from Utils.read_historic_data import merge_csv_data, read_binance_kline_csv_folder
import matplotlib.pyplot as plt
import numpy as np
from Signals.Long.LongSignalDataTrader_StochasticRSIMACD import LongSignalDataTrader_StochasticRSIMACD
from PositionManagers.PositionManagerHistorical import PositionManagerHistorical
from Utils.utils import merge_price_dicts, plot_candles_price_dict, swing_extrema

interval_milliseconds = {"30m": 30 * 60 * 1000,
                         "1m": 1 * 60 * 1000,
                         "1h": 1 * 60 * 60 * 1000,
                         "2h": 2 * 60 * 60 * 1000,
                         "1d": 24 * 60 * 60 * 1000,
                         "3d": 3 * 24 * 60 * 60 * 1000}

symbol = "ETHEUR"
interval = "30m"
years = ["2022"]
csv_data = [read_binance_kline_csv_folder(f"/home/andy/trader/historical_data/{symbol}-{interval}-{year}") for year in years]

csv_data = merge_csv_data(csv_data)

starting_budget = 100
hodl_sell_value = (starting_budget / csv_data["close_price"][0]) * csv_data["close_price"][-1]

capital_fraction = 0.1
profit_target_fraction = 0.1
stop_loss_fraction = 0.05

long_signal = LongSignalDataTrader_StochasticRSIMACD()

position_manager = PositionManagerHistorical(wallet=starting_budget, n_position_limit=1)

n_ticks = csv_data["close_price"].size

heikin_ashi_data = {"close_price": np.array([]), "open_price": np.array([]), "low_price": np.array([]), "high_price": np.array([]), "close_time": np.array([])}

last_red_heikin_ashi_candle = None
last_green_heikin_ashi_candle = None

swing_high_indices = []
swing_low_indices = []



for i in range(n_ticks):
    if i < n_ticks - 1:
        # This simulates incoming ticks

        # We get a tick and store it in an array with previous ticks
        price_data = {"symbol": symbol,
                    "close_price": csv_data["close_price"][:i + 1], 
                    "low_price": csv_data["low_price"][:i + 1], 
                    "high_price": csv_data["high_price"][:i + 1],
                    "close_time": csv_data["close_time"][:i + 1],
                    "open_price": csv_data["open_price"][:i + 1]}
        
        # This is the last tick
        price_data_last = {"symbol": symbol,
                        "close_price": csv_data["close_price"][i],
                        "low_price": csv_data["low_price"][i],
                        "high_price": csv_data["high_price"][i],
                        "close_time": csv_data["close_time"][i],
                        "open_price": csv_data["open_price"][i]}
        

        heikin_ashi_kline = indicator.heikin_ashi_candle(price_data=price_data, heikin_ashi_data=heikin_ashi_data)
        if heikin_ashi_kline["close_price"] < heikin_ashi_kline["open_price"]:
            last_red_heikin_ashi_candle = heikin_ashi_kline
        else:
            last_green_heikin_ashi_candle = heikin_ashi_kline
        merge_price_dicts(heikin_ashi_data, heikin_ashi_kline)
        
        swing_high_idx = swing_extrema(price_data["high_price"], period=20, high_or_low="high")
        if swing_high_idx > -1:
            swing_high_indices.append(swing_high_idx)

    
        swing_low_idx = swing_extrema(price_data["low_price"], period=20, high_or_low="low")
        if swing_low_idx > -1:
            swing_low_indices.append(swing_low_idx)
        
        
        if long_signal.signal_to_long(price_data):
              
                stop_loss_price = (1 - stop_loss_fraction) * price_data_last["close_price"]
                profit_target_price = (1 + profit_target_fraction) * price_data_last["close_price"]

                #if last_red_heikin_ashi_candle:
                    #stop_loss_price = last_red_heikin_ashi_candle["low_price"]
                    #profit_target_price = 2 * (price_data_last["close_price"] - stop_loss_price) + price_data_last["close_price"]

                '''
                if len(swing_low_indices) > 0:
                    nearest_swing_low = price_data["close_price"][swing_low_indices[-1]]

                    if nearest_swing_low < price_data_last["close_price"]:
                        stop_loss_price = nearest_swing_low
                        #profit_target_price = price_data_last["close_price"] + (1.0 * (price_data_last["close_price"] - stop_loss_price))
                        #profit_target_price = 2 * (price_data_last["close_price"] - stop_loss_price) + price_data_last["close_price"]
                '''
                #stop_loss_price = -1
                opened_position = position_manager.try_open_long_position(price_data=price_data_last, 
                                                        capital_fraction=capital_fraction,
                                                        profit_target_price=profit_target_price,
                                                        stop_loss_price=stop_loss_price)
        

        position_manager.try_close_positions(price_data=price_data_last)




print(f"Capital fraction: {capital_fraction}, Start: {starting_budget}, End: {position_manager.wallet}, ROI {100 * ((position_manager.wallet / starting_budget) - 1)} %")
if symbol in position_manager.open_long_positions:
    open_long_positions_value = sum([position.buy_value for position in position_manager.open_long_positions[symbol]])
    print(f"N open long positions: {len(position_manager.open_long_positions[symbol])} with a value of {open_long_positions_value}. Which is {100 * (open_long_positions_value / position_manager.wallet)}% of our current wallet.")
    total_value = position_manager.wallet + open_long_positions_value
    print(f"Including open positions value: End: {total_value}, ROI: {100 * ((total_value / starting_budget) - 1)} %")
if symbol in position_manager.open_short_positions:
    open_short_positions_value = sum([position.buy_value for position in position_manager.open_short_positions[symbol]])
    print(f"N open short positions: {len(position_manager.open_short_positions[symbol])} with a value of {open_short_positions_value}. Which is {100 * (open_short_positions_value / position_manager.wallet)}% of our current wallet.")
if symbol in position_manager.win_rate:
    print(f"Win rate for {symbol}: {position_manager.win_rate[symbol] * 100}%")
print(f"HODL value: {hodl_sell_value}, ROI: {100 * ((hodl_sell_value / starting_budget) - 1)} %")



plt.figure()
plt.title("Normal Candles")
plt.plot(csv_data["close_time"], csv_data["close_price"], "--", label="Close Price")

plot_candles_price_dict(csv_data, width=interval_milliseconds[interval])

#plt.plot(csv_data["close_time"][swing_high_indices], csv_data["high_price"][swing_high_indices], "g^")
#plt.plot(csv_data["close_time"][swing_low_indices], csv_data["low_price"][swing_low_indices], "rv")

if symbol in position_manager.closed_long_positions:
    for closed_position in position_manager.closed_long_positions[symbol]:
        plot_color = "g"
        if closed_position.net_value < 0:
            plot_color = "r"
        
        plt.plot([closed_position.open_time, closed_position.close_time], [closed_position.open_price, closed_position.close_price], plot_color + "-")
        plt.plot([closed_position.open_time, closed_position.close_time], [closed_position.open_price, closed_position.close_price], plot_color + "o")
        
        plt.plot([closed_position.open_time, closed_position.open_time], [closed_position.open_price, closed_position.stop_loss_price], "--",  color="tab:orange")
        plt.hlines(closed_position.stop_loss_price, xmin=closed_position.open_time, xmax=closed_position.close_time, color="tab:orange")

        plt.plot([closed_position.open_time, closed_position.open_time], [closed_position.open_price, closed_position.profit_target_price], "--",  color="tab:olive")
        plt.hlines(closed_position.profit_target_price, xmin=closed_position.open_time, xmax=closed_position.close_time, color="tab:olive")



if symbol in position_manager.closed_short_positions:
    for closed_position in position_manager.closed_short_positions[symbol]:
        plot_color = "g"
        if closed_position.net_value < 0:
            plot_color = "r"
        
        plt.plot([closed_position.open_time, closed_position.close_time], [closed_position.open_price, closed_position.close_price], plot_color + "--")
        plt.plot([closed_position.open_time, closed_position.close_time], [closed_position.open_price, closed_position.close_price], plot_color + "s")
        
        plt.plot([closed_position.open_time, closed_position.open_time], [closed_position.open_price, closed_position.stop_loss_price], "--",  color="tab:orange")
        plt.hlines(closed_position.stop_loss_price, xmin=closed_position.open_time, xmax=closed_position.close_time, color="tab:orange")

plt.legend()


plt.figure()
plt.title("Heikin-Ashi candles")
plt.plot(csv_data["close_time"], csv_data["close_price"], "--", label="Close Price")

plot_candles_price_dict(heikin_ashi_data, width=interval_milliseconds[interval])

#plt.plot(csv_data["close_time"][swing_high_indices], csv_data["high_price"][swing_high_indices], "g^")
#plt.plot(csv_data["close_time"][swing_low_indices], csv_data["low_price"][swing_low_indices], "rv")

if symbol in position_manager.closed_long_positions:
    for closed_position in position_manager.closed_long_positions[symbol]:
        plot_color = "g"
        if closed_position.net_value < 0:
            plot_color = "r"
        
        plt.plot([closed_position.open_time, closed_position.close_time], [closed_position.open_price, closed_position.close_price], plot_color + "-")
        plt.plot([closed_position.open_time, closed_position.close_time], [closed_position.open_price, closed_position.close_price], plot_color + "o")
        
        plt.plot([closed_position.open_time, closed_position.open_time], [closed_position.open_price, closed_position.stop_loss_price], "--",  color="tab:orange")
        plt.hlines(closed_position.stop_loss_price, xmin=closed_position.open_time, xmax=closed_position.close_time, color="tab:orange")

        plt.plot([closed_position.open_time, closed_position.open_time], [closed_position.open_price, closed_position.profit_target_price], "--",  color="tab:olive")
        plt.hlines(closed_position.profit_target_price, xmin=closed_position.open_time, xmax=closed_position.close_time, color="tab:olive")



if symbol in position_manager.closed_short_positions:
    for closed_position in position_manager.closed_short_positions[symbol]:
        plot_color = "g"
        if closed_position.net_value < 0:
            plot_color = "r"
        
        plt.plot([closed_position.open_time, closed_position.close_time], [closed_position.open_price, closed_position.close_price], plot_color + "--")
        plt.plot([closed_position.open_time, closed_position.close_time], [closed_position.open_price, closed_position.close_price], plot_color + "s")
        
        plt.plot([closed_position.open_time, closed_position.open_time], [closed_position.open_price, closed_position.stop_loss_price], "--",  color="tab:orange")
        plt.hlines(closed_position.stop_loss_price, xmin=closed_position.open_time, xmax=closed_position.close_time, color="tab:orange")

plt.legend()


plt.show()
