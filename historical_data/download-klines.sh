#!/bin/bash

# This is a simple script to download klines by given parameters.

while getopts s:i:y: flag
do
    case "${flag}" in
        s) symbol=${OPTARG};;
        i) interval=${OPTARG};;
        y) year=${OPTARG};;
    esac
done

echo ${symbol}
echo ${interval}
echo ${year}

months=(01 02 03 04 05 06 07 08 09 10 11 12)
script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

baseurl="https://data.binance.vision/data/spot/monthly/klines"

outputdir_name="${script_dir}/${symbol}-${interval}-${year}"
if ! [ -d ${outputdir_name} ]; then
    mkdir ${outputdir_name}
fi


for month in ${months[@]}; do
    url="${baseurl}/${symbol}/${interval}/${symbol}-${interval}-${year}-${month}.zip"

    filename="${outputdir_name}/${symbol}-${interval}-${year}-${month}"
    filename_zip="${filename}.zip"
    filename_csv="${filename}.csv"


    if ! [ -f ${filename_csv} ]; then
        response=$(wget -P ${outputdir_name} --server-response -q ${url} 2>&1 | awk 'NR==1{print $2}')
        if [ ${response} == '404' ]; then
            echo "File not exist: ${url}" 
        else
            echo "downloaded: ${url}"
            if [ -f ${filename_zip} ]; then
                unzip ${filename_zip} -d ${outputdir_name}
                echo "Unzipped: ${filename_zip}"
                rm ${filename_zip}
                echo "Deleted zip file ${filename_zip}"
            fi
        fi
    fi
done