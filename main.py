from ExchangeAPIs.Binance import Binance, ExecutionOrderType, UserDataStreamType, read_binance_user_data_stream, user_data_stream_to_human_text, KlineInterval, OrderStatus, OrderSide
from Positions.DBPositionModels import Base
from PositionManagers.PositionManagerBinance import PositionManagerBinance
from Signals.Long.LongSignalDataTrader_StochasticRSIMACD import LongSignalDataTrader_StochasticRSIMACD
from Utils.telegrambot import send_telegram_message
from Utils.utils import rewrite_binance_kline_data
from TickingDataSources.TickingDataSourceWebSocket import TickingDataSourceWebSocket
import secret_keys

import numpy as np
from sqlalchemy import create_engine

from queue import Queue
import time
import logging

def deque_push_back_np_inplace(np_array, push_back_val):
    np_array[0:-1] = np_array[1:]
    np_array[-1] = push_back_val


def store_tick(tick_data):

    if tick_data["k"]["x"]:
        # Kline is closed
        tick_data_to_store = rewrite_binance_kline_data(tick_data["k"])
    
        print(f"Storing: {tick_data_to_store}")

        price_types_to_store = ["close_price", "open_price", "low_price", "high_price"]
        for price_type in price_types_to_store:
            if tick_data_storage[price_type].size < max_tick_storage:
                tick_data_storage[price_type] = np.append(tick_data_storage[price_type], tick_data_to_store[price_type])
            else:
                deque_push_back_np_inplace(tick_data_storage[price_type], tick_data_to_store[price_type])


def on_tick(tick_data=None):

    def process_tick():
        store_tick(tick_data=tick_data)
        kline_data = rewrite_binance_kline_data(tick_data["k"])
        print(kline_data)
        if long_signal.signal_to_long(tick_data_storage):
            close_price = float(kline_data["close_price"])
            position_manager.try_open_long_position(price_data=kline_data, capital_fraction=capital_fraction_on_buy,
                                                    profit_target_price=(1 + profit_fraction) * close_price,
                                                    stop_loss_trigger_price=(1 - stop_loss_trigger_fraction) * close_price,
                                                    stop_loss_price=(1 - stop_loss_fraction) * close_price)
    
    tasks.put(process_tick)


def on_user_data_tick(tick_data=None):

    def process_user_data_tick():
        
        user_data_tick = read_binance_user_data_stream(tick_data=tick_data)

        print(user_data_stream_to_human_text(tick_data=tick_data))
        logging.info(f"User data stream: {user_data_stream_to_human_text(tick_data=tick_data)}")

        if enable_telegram:
            send_telegram_message(user_data_stream_to_human_text(tick_data=tick_data))

        if user_data_tick["event_type"] == UserDataStreamType.EXECUTION_REPORT.value:
            if user_data_tick["side"] == OrderSide.SELL.value:
                if user_data_tick["current_execution_type"] == ExecutionOrderType.TRADE.value:
                    if user_data_tick["current_order_status"] in [OrderStatus.FILLED.value, OrderStatus.PARTIALLY_FILLED.value]:
                        position_manager.fill_open_long_position(order_id=int(user_data_tick["order_id"]),
                                                                fill_amount=float(user_data_tick["cumulative_filled_quantity"]),
                                                                fill_value=float(user_data_tick["cumulative_quote_asset_transacted_quantity"]),
                                                                fill_time=int(user_data_tick["transaction_time"]))
    tasks.put(process_user_data_tick)


if __name__ == "__main__":

    # USER CONFIG
    enable_telegram = True
    enable_sqlitedb = True
    base_currency = "ETH"
    quote_currency = "EUR"
    candle_close_interval = KlineInterval.ONE_MINUTE.value
    max_tick_storage = 100
    long_signal = LongSignalDataTrader_StochasticRSIMACD()
    capital_fraction_on_buy = 0.02 # Fraction of current quote currency used when opening a position
    profit_fraction = 0.10 # Percentage increase of buy price until take profit order i.e. (1 + profit_fraction) * buy_price = take_profit_price
    stop_loss_trigger_fraction = 0.20 # Percentage decrease of buy price until stop loss order is placed i.e. (1 - stop_loss_trigger_fraction) * buy_price = stop_loss_trigger_price
    stop_loss_fraction = 0.30 # Percentage decrease of buy price where stop loss order is placed i.e. (1 - stop_loss_fraction) * buy_price = stop_loss_price
    ##########################

    logging.basicConfig(level=logging.INFO, filename="main_" + base_currency + quote_currency + ".log", 
                        format='%(process)d-%(asctime)s-%(levelname)s-%(message)s')

    bot_creation_time = time.time()
    tick_data_storage = {"close_price": np.array([]), 
                         "open_price": np.array([]), 
                         "low_price": np.array([]), 
                         "high_price": np.array([])}

    # Initialize Binance API object
    binance = Binance(api_endpoint=f"https://api.binance.com", 
                    api_key=secret_keys.binance_main_apikey, 
                    api_secret=secret_keys.binance_main_secret)
    tasks = Queue()

    # Initialize sqlite DB
    if enable_sqlitedb:
        mainnet_engine = create_engine("sqlite+pysqlite:///main_" + base_currency + quote_currency + ".db", echo=True)
        Base.metadata.create_all(mainnet_engine)
    db_engine = mainnet_engine if enable_sqlitedb else None

    # Initialize Binance position manager
    position_manager = PositionManagerBinance(binance=binance, wallet_asset=quote_currency, 
                                              n_position_limit=1, db_engine=db_engine, 
                                              enable_telegram=enable_telegram)

    # Initialize user data stream listener (receives changes on the user account and orders)
    binance_user_data_stream_listenkey = binance.create_spot_listen_key()["listenKey"]
    listen_key_start_time = time.time()
    binance_user_data_stream = f"wss://stream.binance.com:9443/ws/" + binance_user_data_stream_listenkey
    binance_user_data_stream_ticker = TickingDataSourceWebSocket(stream_url=binance_user_data_stream)
    binance_user_data_stream_ticker.attach_fun_to_tick(on_user_data_tick)
    binance_user_data_stream_ticker.start()

    # Initialize the kline ticker for the defined currency pair and ticker interval
    symbol = base_currency + quote_currency
    binance_stream = f"wss://stream.binance.com:9443/ws/{symbol.lower()}@kline_{candle_close_interval}"
    binance_ticker = TickingDataSourceWebSocket(stream_url=binance_stream)
    binance_ticker.attach_fun_to_tick(on_tick)
    binance_ticker.start()

    while True:
        try:
            tasks.get_nowait()()
        except:
            pass
        
        # Every 30 mins refresh the user data stream listen key. It expires after 1 hour
        if time.time() - listen_key_start_time >= 30 * 60:
            binance.keep_alive_spot_listen_key(listenKey=binance_user_data_stream_listenkey)
            listen_key_start_time = time.time()
