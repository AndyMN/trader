from abc import ABC, abstractmethod
import threading

class BaseTickingDataSource(ABC, threading.Thread):

    def __init__(self) -> None:
        threading.Thread.__init__(self)
        ABC.__init__(self)
        self.tick_observers = []
        self.tick_observer_functions = []

    def attach_fun_to_tick(self, observing_fun):
        self.tick_observer_functions.append(observing_fun)

    def attach_to_tick(self, observer):
        if observer not in self.tick_observers:
            self.tick_observers.append(observer)

    def detach_from_tick(self, observer):
        if observer in self.tick_observers:
            self.tick_observers.remove(observer)

    def detach_fun_from_tick(self, observing_fun):
        self.tick_observer_functions.remove(observing_fun)

    def on_tick(self, tick_data):
        for observer in self.tick_observers:
            observer.on_tick(tick_data)

        for observing_fun in self.tick_observer_functions:
            observing_fun(tick_data=tick_data)

    @abstractmethod
    def run(self):
        pass
