from TickingDataSources.BaseTickingDataSource import BaseTickingDataSource
import websocket, json


class TickingDataSourceWebSocket(BaseTickingDataSource):

    def __init__(self, stream_url):
        BaseTickingDataSource.__init__(self)
        self.stream_url = stream_url

        self.close_fun = None
    
    def attach_fun_to_close(self, fun):
        self.close_fun = fun

    def on_close(self, ws, close_status_code, close_status_msg):
        if self.close_fun:
            self.close_fun(ws, close_status_code, close_status_msg)
    
    def on_message(self, ws, message):
        message_json = json.loads(message)

        self.on_tick(message_json)

    def on_error(self, this_object, exception):
        print(exception)

    def run(self):
        print("Ticker started !")
        ws = websocket.WebSocketApp(self.stream_url, on_error=self.on_error, on_message=self.on_message, on_close=self.on_close)
        ws.run_forever()