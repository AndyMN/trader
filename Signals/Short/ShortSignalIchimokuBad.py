from Signals.Short.BaseShortSignal import BaseShortSignal
import Utils.indicator as indicator

class ShortSignalIchimokuBad(BaseShortSignal):

    def __init__(self, conversion_period=9, base_period=26):
        BaseShortSignal.__init__(self)
        self.conversion_period = conversion_period
        self.base_period = base_period

        self.leading_B_over_A = False
    
    def signal_to_short(self, price_data):
        ichimoku_data = indicator.ichimoku(close_price=price_data["close_price"],
                                            low_price=price_data["low_price"],
                                            high_price=price_data["high_price"],
                                            conversion_period=self.conversion_period,
                                            base_period=self.base_period)        


        if ichimoku_data["leading_span_B"] > ichimoku_data["leading_span_A"]:
            if not self.leading_B_over_A:
                self.leading_B_over_A = True
                return True
        else:
            self.leading_B_over_A = False
            return False
