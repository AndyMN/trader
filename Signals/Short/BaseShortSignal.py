from abc import ABC, abstractmethod

class BaseShortSignal(ABC):

    @abstractmethod
    def signal_to_short(self, price_data):
        return False




