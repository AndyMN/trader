from Signals.Long.BaseLongSignal import BaseLongSignal
import Utils.indicator as indicator

class LongSignalDataTrader_StochasticRSIMACD(BaseLongSignal):
    # https://www.youtube.com/watch?v=hh3BKTFE1dc

    def __init__(self, stochastic_Kperiod=14, stochastic_Ksmoothperiod=3, 
                stochastic_Dperiod=3, rsi_period=14, 
                macd_short_period=12, macd_long_period=26, 
                macd_signal_period=9):

                BaseLongSignal.__init__(self)

                self.stochastic_Kperiod = stochastic_Kperiod
                self.stochastic_Ksmoothperiod = stochastic_Ksmoothperiod
                self.stochastic_Dperiod = stochastic_Dperiod

                self.rsi_period = rsi_period

                self.macd_short_period = macd_short_period
                self.macd_long_period = macd_long_period
                self.macd_signal_period = macd_signal_period

                self.stochastic_overbought = False
                self.stochastic_oversold = False
    

    def signal_to_long(self, price_data, data=None):
        

        stochastic_data = indicator.stochastic(price_data["close_price"], price_data["low_price"], price_data["high_price"], 
                                               n_Kperiods=self.stochastic_Kperiod, n_Ksmoothperiods=self.stochastic_Ksmoothperiod,
                                               n_Dperiods=self.stochastic_Dperiod)

        rsi = indicator.rsi(price_data["close_price"], period=self.rsi_period)

        macd_data = indicator.macd(price_data["close_price"], short_term_period=self.macd_short_period, 
                                   long_term_period=self.macd_long_period, signal_period=self.macd_signal_period)


        if stochastic_data["K"] < 20 and stochastic_data["D"] < 20:
            self.stochastic_oversold = True
            self.stochastic_overbought = False

        if stochastic_data["K"] > 80 and stochastic_data["D"] > 80:
            self.stochastic_overbought = True
            self.stochastic_oversold = False
        
        if self.stochastic_oversold:
            if rsi > 50:
                if macd_data["macd"] > macd_data["signal"]:
                    return True
        
        return False
