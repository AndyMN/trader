from abc import ABC, abstractmethod

class BaseLongSignal(ABC):

    @abstractmethod
    def signal_to_long(self, price_data, data=None):
        return False



