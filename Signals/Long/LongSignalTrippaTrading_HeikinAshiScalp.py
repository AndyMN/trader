from Utils.utils import merge_price_dicts
import Utils.indicator as indicator
from Signals.Long.BaseLongSignal import BaseLongSignal

import numpy as np
class LongSignalTrippaTrading_HeikinAshiScalp(BaseLongSignal):
    # https://www.youtube.com/watch?v=9NG0hnwO70g
    # This is for 1m intervals

    def __init__(self, rsi_period=14):
        BaseLongSignal.__init__(self)

        self.rsi_period = rsi_period
        self.rsi_oversold = False

        self.heikin_ashi_data = {"open_price": np.array([]), "close_price": np.array([]), "low_price": np.array([]), "high_price": np.array([])}

    def signal_to_long(self, price_data, price_data_4h):
        
        # Using Heikin-Ashi candles
        heikin_ashi_kline = indicator.heikin_ashi_candle(price_data=price_data, heikin_ashi_data=self.heikin_ashi_data)
        merge_price_dicts(self.heikin_ashi_data, heikin_ashi_kline)


        # EWMA 50 period of the 4 hour chart
        long_period = 50
        if price_data_4h["close_price"].size >= long_period:

            smoothing_alpha_long = 2.0 / (long_period + 1)
            ema50 = indicator.EWMA(price_data_4h["close_price"][-long_period:], smoothing_alpha=smoothing_alpha_long)

            if price_data["close_price"][-1] > ema50:
                # We can only open long positions when the price moves above the ema50

                # RSI
                rsi = indicator.rsi(price_data["close_price"], period=self.rsi_period)
                if rsi < 30:
                    self.rsi_oversold = True
                else:
                    self.rsi_oversold = False
                
                if self.rsi_oversold:
                    if self.heikin_ashi_data["close_price"][-1] > self.heikin_ashi_data["open_price"][-1]:
                        # Green Heikin-Ashi candle

                        # EWMA 9 period of the 1m chart (current)
                        short_period = 9
                        smoothing_alpha_short = 2.0 / (short_period + 1)
                        ema9 = indicator.EWMA(price_data["close_price"][-short_period:], smoothing_alpha=smoothing_alpha_short)

                        if self.heikin_ashi_data["close_price"][-1] > ema9:
                            return True

        return False