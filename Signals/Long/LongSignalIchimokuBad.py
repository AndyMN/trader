from Signals.Long.BaseLongSignal import BaseLongSignal
import Utils.indicator as indicator

class LongSignalIchimokuBad(BaseLongSignal):

    def __init__(self, conversion_period=9, base_period=26):
        BaseLongSignal.__init__(self)
        self.conversion_period = conversion_period
        self.base_period = base_period

        self.leading_A_over_B = False

    def signal_to_long(self, price_data, data=None):
        ichimoku_data = indicator.ichimoku(close_price=price_data["close_price"],
                                            low_price=price_data["low_price"],
                                            high_price=price_data["high_price"],
                                            conversion_period=self.conversion_period,
                                            base_period=self.base_period)        


        if ichimoku_data["leading_span_A"] > ichimoku_data["leading_span_B"]:
            if not self.leading_A_over_B:
                self.leading_A_over_B = True
                return True
        else:
            self.leading_A_over_B = False
            return False
