from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Float, Integer, String

from datetime import datetime

from Positions.Positions import OpenLongPosition, ClosedLongPosition

Base = declarative_base()

class DBOpenLongPosition(Base):

    __tablename__ = "OpenLongPositions"

    id = Column(Integer, primary_key=True)
    
    symbol = Column(String)

    take_profit_order_id = Column(Integer)
    stop_loss_order_id = Column(Integer)
    oco_list_id = Column(Integer)
    
    open_time_unix = Column(Integer)
    open_time = Column(String)

    buy_amount = Column(Float)
    buy_value = Column(Float)
    buy_price = Column(Float)

    filled_amount = Column(Float)
    sell_value = Column(Float)

    take_profit_price = Column(Float)
    stop_loss_price = Column(Float)
    stop_loss_trigger_price = Column(Float)

  
    def __init__(self, open_long_position: OpenLongPosition) -> None:
        super().__init__()

        self.symbol = open_long_position.symbol

        self.take_profit_order_id = open_long_position.take_profit_order_id
        self.stop_loss_order_id = open_long_position.stop_loss_order_id
        self.oco_list_id = open_long_position.oco_list_id
        
        self.open_time_unix = open_long_position.open_time_unix
        self.open_time = open_long_position.open_time_human

        self.buy_amount = open_long_position.buy_amount
        self.buy_value = open_long_position.buy_value
        self.buy_price = open_long_position.buy_price

        self.filled_amount = open_long_position.actual_filled_amount
        self.sell_value = open_long_position.sell_value

        self.take_profit_price = open_long_position.take_profit_price
        self.stop_loss_price = open_long_position.stop_loss_price
        self.stop_loss_trigger_price = open_long_position.stop_loss_trigger_price
        
    def __repr__(self):

        repr = f"Position for symbol: {self.symbol}\n"
        repr += f"Stop loss order ID: {self.stop_loss_order_id}\n"
        repr += f"Take profit order ID: {self.take_profit_order_id}\n"
        repr += f"Open time: {self.open_time}\n"
        repr += f"Buy value: {self.buy_value}\n"
        repr += f"Buy amount: {self.buy_amount}\n"
        repr += f"Buy price: {self.buy_price}\n"
        repr += f"Profit target price: {self.take_profit_price}\n"
        repr += f"Stop loss price: {self.stop_loss_price}\n"
        repr += f"Stop loss trigger price: {self.stop_loss_trigger_price}."
            
        return repr

    

class DBClosedLongPosition(Base):

    __tablename__ = "ClosedLongPositions"

    id = Column(Integer, primary_key=True)

    symbol = Column(String)

    take_profit_order_id = Column(Integer)
    stop_loss_order_id = Column(Integer)
    oco_list_id = Column(Integer)
    
    open_time_unix = Column(Integer)
    open_time = Column(String)

    close_time_unix = Column(Integer)
    close_time = Column(String)

    buy_amount = Column(Float)
    buy_value = Column(Float)

    filled_amount = Column(Float)
    sell_value = Column(Float)

    take_profit_price = Column(Float)
    stop_loss_price = Column(Float)

    net_value = Column(Float)

    def __init__(self, closed_long_position: ClosedLongPosition):
        super().__init__()

        self.symbol = closed_long_position.symbol

        self.take_profit_order_id = closed_long_position.take_profit_order_id
        self.stop_loss_order_id = closed_long_position.stop_loss_order_id
        self.oco_list_id = closed_long_position.oco_list_id
        
        self.open_time_unix = closed_long_position.open_time_unix
        self.open_time = closed_long_position.open_time_human

        self.close_time_unix = closed_long_position.close_time_unix
        self.close_time = datetime.fromtimestamp(self.close_time_unix / 1000).strftime("%Y-%m-%d %H:%M:%S")

        self.buy_amount = closed_long_position.buy_amount
        self.buy_value = closed_long_position.buy_value

        self.filled_amount = closed_long_position.actual_filled_amount
        self.sell_value = closed_long_position.sell_value

        self.take_profit_price = closed_long_position.take_profit_price
        self.stop_loss_price = closed_long_position.stop_loss_price

        self.net_value = self.sell_value - self.buy_value

    def __repr__(self):
    
        repr = f"Closed position for symbol: {self.symbol}\n"
        repr += f"Stop loss order ID: {self.stop_loss_order_id}\n"
        repr += f"Take profit order ID: {self.take_profit_order_id}\n"
        repr += f"Open time: {self.open_time}\n"
        repr += f"Close time: {self.close_time}\n"
        repr += f"Buy value: {self.buy_value}\n"
        repr += f"Actual filled amount: {self.filled_amount}\n"
        repr += f"Profit target price: {self.take_profit_price}\n"
        repr += f"Stop loss price: {self.stop_loss_price}\n"
        repr += f"Sell value: {self.sell_value}\n"
        repr += f"Net value: {self.net_value}\n"
        
        return repr
