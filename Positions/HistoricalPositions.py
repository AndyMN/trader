from abc import ABC

class Position(ABC):

    def __init__(self, wallet_before_open, wallet_after_open, symbol, open_price, open_time, profit_target_price, stop_loss_price):
        
        self.wallet_before_open = wallet_before_open
        self.wallet_after_open = wallet_after_open
        self.wallet_before_close = 0
        self.wallet_after_close = 0

        self.symbol = symbol
        self.open_price = open_price
        self.open_time = open_time
        self.profit_target_price = profit_target_price
        self.stop_loss_price = stop_loss_price

        self.sell_value = 0
        self.buy_value = 0
        self.net_value = 0

        self.profit_target = 0
        self.stop_loss = 0

        self.close_price = 0
        self.close_time = 0
    
    def close(self, close_price, close_time, wallet_before_close, wallet_after_close):
        self.close_price = close_price
        self.close_time = close_time

        self.wallet_before_close = wallet_before_close
        self.wallet_after_close = wallet_after_close

        self.net_value = self.sell_value - self.buy_value

    def __repr__(self):
        return f'''Position for symbol: {self.symbol}, 
                Open price: {self.open_price}, Open time: {self.open_time},
                Buy value: {self.buy_value}, Buy amount: {self.buy_amount},
                Profit target price: {self.profit_target_price}, Stop loss price: {self.stop_loss_price},
                Profit target: {self.profit_target}, Stop loss: {self.stop_loss},
                Close price: {self.close_price}, Close time: {self.close_time},
                Sell value: {self.sell_value}, Net value: {self.net_value},
                Wallet value: Before Open: {self.wallet_before_open}, After Open: {self.wallet_after_open},
                Before Close: {self.wallet_before_close}, After Close: {self.wallet_after_close}'''


class LongPosition(Position):

    def __init__(self, wallet_before_open, wallet_after_open, symbol, open_price, open_time, profit_target_price, stop_loss_price, buy_value, buy_amount):
        Position.__init__(self, wallet_before_open, wallet_after_open, symbol, open_price, open_time, profit_target_price, stop_loss_price)

        self.buy_value = buy_value
        self.buy_amount = buy_amount

        self.profit_target = self.buy_amount * self.profit_target_price
        self.stop_loss = self.buy_amount * self.stop_loss_price
    

    def close(self, close_price, close_time, wallet_before_close, wallet_after_close, sell_value):
        self.sell_value = sell_value
        
        Position.close(self, close_price, close_time, wallet_before_close, wallet_after_close)
    
    def __repr__(self):

        return "Long Position Info: " + super.__repr__(self)


class ShortPosition(Position):

    def __init__(self, wallet_before_open, wallet_after_open, symbol, open_price, open_time, profit_target_price, stop_loss_price, sell_value, buy_amount):
        Position.__init__(self, wallet_before_open, wallet_after_open, symbol, open_price, open_time, profit_target_price, stop_loss_price)

        self.sell_value = sell_value
        self.buy_amount = buy_amount
        
        self.profit_target = self.buy_amount * self.profit_target_price
        self.stop_loss = self.buy_amount * self.stop_loss_price
    
    def close(self, close_price, close_time, wallet_before_close, wallet_after_close, buy_value):
        self.buy_value = buy_value

        Position.close(self, close_price, close_time, wallet_before_close, wallet_after_close)
    
    def __repr__(self):

        return "Short Position Info: " + super.__repr__(self)

