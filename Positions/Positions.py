from datetime import datetime

class OpenLongPosition:

    def __init__(self, symbol, take_profit_order_id, stop_loss_order_id, oco_list_id, 
                open_time, buy_amount, buy_value, buy_price, take_profit_price, stop_loss_price, 
                stop_loss_trigger_price):

        self.symbol = symbol
        
        self.take_profit_order_id = take_profit_order_id
        self.stop_loss_order_id = stop_loss_order_id
        self.oco_list_id = oco_list_id
        self.open_time_unix = open_time

        self.open_time_human = datetime.fromtimestamp(self.open_time_unix / 1000).strftime("%Y-%m-%d %H:%M:%S")

        self.buy_amount = buy_amount
        self.buy_value = buy_value
        self.buy_price = buy_price
        
        # The actual amount that was filled in the order
        self.actual_filled_amount = 0
        self.sell_value = 0

        self.take_profit_price = take_profit_price
        self.stop_loss_price = stop_loss_price
        self.stop_loss_trigger_price = stop_loss_trigger_price

        self.is_open = True

    def fill_position(self, fill_amount, fill_value):
        if self.is_open:
            self.actual_filled_amount = fill_amount
            self.sell_value = fill_value

            if self.actual_filled_amount == self.buy_amount:
                self.is_open = False
            else:
                print(f"{self.actual_filled_amount} is not the same as {self.buy_amount}")

    def __repr__(self):

        repr = f"Position for symbol: {self.symbol}\n"
        repr += f"Stop loss order ID: {self.stop_loss_order_id}\n"
        repr += f"Take profit order ID: {self.take_profit_order_id}\n"
        repr += f"Open time: {self.open_time_human}\n"
        repr += f"Buy value: {self.buy_value}\n"
        repr += f"Buy amount: {self.buy_amount}\n"
        repr += f"Buy price: {self.buy_price}\n"
        repr += f"Profit target price: {self.take_profit_price}\n"
        repr += f"Stop loss price: {self.stop_loss_price}\n"
        repr += f"Stop loss trigger price: {self.stop_loss_trigger_price}\n"
        repr += f"IsOpen: {self.is_open}."
        
        return repr

class ClosedLongPosition:

    def __init__(self, open_long_position: OpenLongPosition, fill_time):
        self.symbol = open_long_position.symbol
        
        self.take_profit_order_id = open_long_position.take_profit_order_id
        self.stop_loss_order_id = open_long_position.stop_loss_order_id
        self.oco_list_id = open_long_position.oco_list_id
        self.open_time_unix = open_long_position.open_time_unix
        self.open_time_human = open_long_position.open_time_human
        
        self.close_time_unix = fill_time
        self.close_time_human = datetime.fromtimestamp(self.close_time_unix / 1000).strftime("%Y-%m-%d %H:%M:%S")

        self.buy_amount = open_long_position.buy_amount
        self.buy_value = open_long_position.buy_value

        # The actual amount that was filled in the order
        self.actual_filled_amount = open_long_position.actual_filled_amount
        self.sell_value = open_long_position.sell_value

        self.take_profit_price = open_long_position.take_profit_price
        self.stop_loss_price = open_long_position.stop_loss_price

        self.net_value = self.sell_value - self.buy_value

    def __repr__(self):

        repr = f"Closed position for symbol: {self.symbol}\n"
        repr += f"Stop loss order ID: {self.stop_loss_order_id}\n"
        repr += f"Take profit order ID: {self.take_profit_order_id}\n"
        repr += f"Open time: {self.open_time_human}\n"
        repr += f"Close time: {self.close_time_human}\n"
        repr += f"Buy value: {self.buy_value}\n"
        repr += f"Actual filled amount: {self.actual_filled_amount}\n"
        repr += f"Profit target price: {self.take_profit_price}\n"
        repr += f"Stop loss price: {self.stop_loss_price}\n"
        repr += f"Sell value: {self.sell_value}\n"
        repr += f"Net value: {self.net_value}\n"
        
        return repr
