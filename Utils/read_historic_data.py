import numpy as np
import os

def read_csv(csv_filename, column_names):

    csv_data = {column_name: [] for column_name in column_names}

    with open(csv_filename) as f:
        for line in f.readlines():
            for i, val in enumerate(line.split(",")):
                csv_data[column_names[i]].append(float(val))
    
    for column_name in column_names:
        csv_data[column_name] = np.array(csv_data[column_name])

    return csv_data


def read_binance_kline_csv(csv_filename):

    kline_column_names = ["open_time", "open_price", "high_price", 
                          "low_price", "close_price", "volume", "close_time", 
                          "quote_asset_volume", "n_trades", 
                          "base_asset_volume", "quote_asset_volume", "ignore"]

    
    return read_csv(csv_filename, kline_column_names)

def merge_csv_data(csv_data_array):
    csv_data = csv_data_array[0]
    for csv_data_entry in csv_data_array[1:]:
        for key in csv_data_entry:
            csv_data[key] = np.append(csv_data[key], csv_data_entry[key])
    
    return csv_data


def read_binance_kline_csv_folder(csv_folder_name):
    csv_data = []
    csv_filenames = [f for f in os.listdir(csv_folder_name) if os.path.splitext(f)[1] == ".csv" ]
    csv_filenames.sort()
    for csv_file in csv_filenames:
        csv_data.append(read_binance_kline_csv(os.path.join(csv_folder_name, csv_file)))

    return merge_csv_data(csv_data)



if __name__ == "__main__":

    csv_data = read_binance_kline_csv_folder("/home/andy/trader/historical_data/ETHEUR-30m-2021")
    print(csv_data["close_price"].size)