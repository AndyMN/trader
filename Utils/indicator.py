import numpy as np

def ichimoku(close_price, low_price, high_price, conversion_period=9, base_period=26):

    ichimoku_data = {"conversion": np.nan,
                     "baseline": np.nan,
                     "leading_span_A": np.nan, 
                     "leading_span_B": np.nan, 
                     "lagging_span": np.nan}
    
    if not (close_price.size == low_price.size) and not (close_price.size == high_price.size):
        print("Ichimoku Indicator: Arrays do not match in size !")
        exit(-1)

    if high_price.size >= conversion_period:
        high_value = np.max(high_price[-conversion_period:])
        low_value = np.min(low_price[-conversion_period:])
        ichimoku_data["conversion"] = 0.5 * (high_value + low_value)

    if high_price.size >= base_period:
        high_value = np.max(high_price[-base_period:])
        low_value = np.min(low_price[-base_period:])
        ichimoku_data["baseline"] = 0.5 * (high_value + low_value)

    if high_price.size >= base_period:
        ichimoku_data["leading_span_A"] = 0.5 * (ichimoku_data["conversion"] + ichimoku_data["baseline"])

    if high_price.size >= 2 * base_period:
    
        high_value = np.max(high_price[-2 * base_period:])
        low_value = np.min(low_price[-2 * base_period:])
        ichimoku_data["leading_span_B"] = 0.5 * (high_value + low_value)

    if close_price.size >= base_period:
        ichimoku_data["lagging_span"] = close_price[-base_period]

    return ichimoku_data


def ichimoku_array(close_price, low_price, high_price, conversion_period=9, base_period=26):

    ichimoku_data = {"conversion": [], 
                     "baseline": [], 
                     "leading_span_A": [], 
                     "leading_span_B": [], 
                     "lagging_span": []}
    
    if not (close_price.size == low_price.size) and not (close_price.size == high_price.size):
        print("Ichimoku Indicator: Arrays do not match in size !")
        exit(-1)

    for i in range(close_price.size):
        ichimoku_data_single_value = ichimoku(close_price=close_price[:i], 
                                                   low_price=low_price[:i], 
                                                   high_price=high_price[:i], 
                                                   conversion_period=conversion_period, 
                                                   base_period=base_period)

        for ichimoku_type, ichimoku_value in ichimoku_data_single_value.items():
            ichimoku_data[ichimoku_type].append(ichimoku_value)
        
    for ichimoku_type in ichimoku_data:
        ichimoku_data[ichimoku_type] = np.array(ichimoku_data[ichimoku_type])

    return ichimoku_data

def stochastic_K(close_price, low_price, high_price, n_Kperiods=14):
    # Fast K
    K_val = np.nan
    if low_price.size >= n_Kperiods:
        # Most recent closing price
        C = close_price[-1]
        # Lowest price previous n_periods
        Lperiods = np.min(low_price[-n_Kperiods:])
        # Highest price previous n_periods
        Hperiods = np.max(high_price[-n_Kperiods:])

        K_val = 100 * (C - Lperiods) / (Hperiods - Lperiods)

    return K_val

def stochastic(close_price, low_price, high_price, n_Kperiods=14, n_Ksmoothperiods=3, n_Dperiods=3):
    # https://school.stockcharts.com/doku.php?id=technical_indicators:stochastic_oscillator_fast_slow_and_full

    stochastic_data = {"K": np.nan, "D": np.nan}

    if not (close_price.size == low_price.size) and not (close_price.size == high_price.size):
        print("Ichimoku Indicator: Arrays do not match in size !")
        exit(-1)

    if close_price.size >= n_Kperiods + n_Ksmoothperiods:

        stochastic_data["K"] = stochastic_K(close_price=close_price, low_price=low_price,
                                            high_price=high_price, n_Kperiods=n_Kperiods)
    
    if low_price.size >= (n_Kperiods + (n_Ksmoothperiods * n_Dperiods)):

        # We need n_Dperiods of smoothed K line values for our D/signal line
        # We need n_Ksmoothperiods of K line values for a smoothed K line value
        # So we need to have n_Kperiod + (n_Dperiod * n_Ksmoothperiod) amount of price datapoints

        K = []
        K_smooth = []

        for j in range(n_Dperiods):
            for i in range(n_Ksmoothperiods):
                if i + j > 0:
                    K.append(stochastic_K(close_price=close_price[:-(i + j)][-n_Kperiods:],
                                        low_price=low_price[:-(i + j)][-n_Kperiods:],
                                        high_price=high_price[:-(i + j)][-n_Kperiods:]))
                else:
                    K.append(stochastic_K(close_price=close_price[-n_Kperiods:],
                                        low_price=low_price[-n_Kperiods:],
                                        high_price=high_price[-n_Kperiods:]))
            
            K_smooth.append(np.average(np.array(K[-n_Ksmoothperiods:])))
        
        stochastic_data["K"] = K_smooth[-1]
        stochastic_data["D"] = np.average(np.array(K_smooth))

    return stochastic_data

def rsi(close_price, period=14):

    rsi_val = np.nan

    if close_price.size > period:
        
        # Close price now > close price previous or 0 i.e. gains
        U = []
        # Close price previous > close price now or 0 i.e. losses
        D = []

        # We want an extra data point or else we cannot calculate a period amount of differences (U and D)
        close_price_period = close_price[-(period + 1):]

        # https://en.wikipedia.org/wiki/Relative_strength_index
        for i in range(period):
            close_now = close_price_period[i + 1]
            close_previous = close_price_period[i]

            if close_now > close_previous:
                U.append(close_now - close_previous)
                D.append(0.0)
            elif close_now < close_previous:
                U.append(0.0)
                D.append(close_previous - close_now)
            else:
                U.append(0.0)
                D.append(0.0)
        
        U = np.array(U)
        D = np.array(D)

        RS = EWMA(U, 1.0 / period) / EWMA(D, 1.0 / period)

        rsi_val = 100 - (100 / (1 + RS))
    
    return rsi_val

def macd_macd(close_price, short_term_period=12, long_term_period=26):

    macd_val = np.nan
    if close_price.size >= long_term_period:
        smoothing_alpha_short = 2.0 / (short_term_period + 1)
        smoothing_alpha_long = 2.0 / (long_term_period + 1)
        macd_val = EWMA(close_price[-short_term_period:], smoothing_alpha_short) - EWMA(close_price[-long_term_period:], smoothing_alpha_long)

    return macd_val

def macd(close_price, short_term_period=12, long_term_period=26, signal_period=9):

    macd_data = {"macd": np.nan, "signal": np.nan}

    if short_term_period > long_term_period:
        print(f"MACD: Short term period {short_term_period} larger than long term period {long_term_period}.")
        exit(-1)

    macd_data["macd"] = macd_macd(close_price, short_term_period=short_term_period, long_term_period=long_term_period)
    
    if close_price.size >= (signal_period + long_term_period):
        smoothing_alpha_signal = 2.0 / (signal_period + 1)

        # Signal line is a signal_period EWMA of the MACD line

        macd_vals = []
        for i in range(signal_period):
            if i > 0:
                macd_vals.append(macd_macd(close_price[:-i], short_term_period=short_term_period, long_term_period=long_term_period))
            else:
                macd_vals.append(macd_macd(close_price, short_term_period=short_term_period, long_term_period=long_term_period))

        macd_vals = np.array(macd_vals)
        np.flip(macd_vals) # Most recent MACD value needs to be last

        macd_data["signal"] = EWMA(macd_vals, smoothing_alpha_signal)

    return macd_data

def EWMA(array, smoothing_alpha):
    # Exponentially weighted moving average
    # https://corporatefinanceinstitute.com/resources/knowledge/trading-investing/exponentially-weighted-moving-average-ewma/#:~:text=The%20Exponentially%20Weighted%20Moving%20Average%20(EWMA)%20is%20a%20quantitative%20or,technical%20analysis%20and%20volatility%20modeling.
    
    if array.size > 1:
        return smoothing_alpha * array[-1] + (1 - smoothing_alpha) * EWMA(array[:-1], smoothing_alpha)
    else:
        return array[0]


def heikin_ashi_candle(price_data, heikin_ashi_data):
    # https://www.investopedia.com/terms/h/heikinashi.asp
    heikin_ashi_kline = {"close_price": 0, "open_price": 0, "low_price": 0, "high_price": 0, "close_time": 0}
    heikin_ashi_kline["close_time"] = price_data["close_time"][-1]

    heikin_ashi_kline["close_price"] = 0.25 * (price_data["open_price"][-1] + price_data["high_price"][-1] + price_data["low_price"][-1] + price_data["close_price"][-1])
    
    if heikin_ashi_data["open_price"].size == 0:
        heikin_ashi_kline["open_price"] = 0.5 * (price_data["open_price"][-1] + price_data["close_price"][-1])
        heikin_ashi_kline["low_price"] = price_data["low_price"][-1]
        heikin_ashi_kline["high_price"] = price_data["high_price"][-1]
    else:
        heikin_ashi_kline["open_price"] = 0.5 * (heikin_ashi_data["open_price"][-1] + heikin_ashi_data["close_price"][-1])
        heikin_ashi_kline["high_price"] = np.max([price_data["high_price"][-1], heikin_ashi_kline["open_price"], heikin_ashi_kline["close_price"]])
        heikin_ashi_kline["low_price"] = np.min([price_data["low_price"][-1], heikin_ashi_kline["open_price"], heikin_ashi_kline["close_price"]])

    return heikin_ashi_kline
