import numpy as np
from Utils.indicator import EWMA

def raw_mini_tick_data_to_cleaned(raw_mini_tick_data):        
    raw_mini_tick_data_labels = {"e": "event_type", "E": "event_time", "s": "symbol",
                                "c": "close_price", "o": "open_price", "h": "high_price",
                                "l": "low_price", "v": "total_base_volume", "q": "total_quote_volume"}

    tick_data = {}

    for short_label, long_label in raw_mini_tick_data_labels.items():
        if short_label in raw_mini_tick_data:
            tick_data[long_label] = raw_mini_tick_data[short_label]
        else:
            tick_data[long_label] = None

    return tick_data


def swing_extrema(array, period, high_or_low="high"):

    if array.size < 2 * period + 1:
        return -1

    swing_array = array[-(2 * period + 1):]
    swing_array_idx_start = array.size - (2 * period + 1)

    if high_or_low == "high":
        max_val_idx = np.argmax(swing_array)
        if max_val_idx == period + 1:
            return max_val_idx + swing_array_idx_start
        else:
            return -1
    elif high_or_low == "low":
        min_val_idx = np.argmin(swing_array)
        if min_val_idx == period + 1:
            return min_val_idx + swing_array_idx_start
        else:
            return -1


def merge_price_dicts(price_dict_destination, price_dict_to_merge):

    for key in price_dict_destination:
        if key not in price_dict_to_merge:
            print("These dicts do not contain the same data !")
            exit(-1)
        
        price_dict_destination[key] = np.append(price_dict_destination[key], price_dict_to_merge[key])

def plot_candles_price_dict(price_dict, width):
    import matplotlib.pyplot as plt

    green = price_dict["close_price"] > price_dict["open_price"]
    red = price_dict["close_price"] < price_dict["open_price"]

    plt.bar(price_dict["close_time"][green], height=price_dict["close_price"][green] - price_dict["open_price"][green], width=width, bottom=price_dict["open_price"][green], color="green")
    plt.bar(price_dict["close_time"][green], height=price_dict["high_price"][green] - price_dict["close_price"][green], width=0.1 * width, bottom=price_dict["close_price"][green], color="green")
    plt.bar(price_dict["close_time"][green], height=price_dict["low_price"][green] - price_dict["open_price"][green], width=0.1 * width, bottom=price_dict["open_price"][green], color="green")

    plt.bar(price_dict["close_time"][red], height=price_dict["close_price"][red] - price_dict["open_price"][red], width=width, bottom=price_dict["open_price"][red], color="red")
    plt.bar(price_dict["close_time"][red], height=price_dict["high_price"][red] - price_dict["open_price"][red], width=0.1 * width, bottom=price_dict["open_price"][red], color="red")
    plt.bar(price_dict["close_time"][red], height=price_dict["low_price"][red] - price_dict["close_price"][red], width=0.1 * width, bottom=price_dict["close_price"][red], color="red")


def rewrite_binance_kline_data(kline_data):

    price_data = {"open_time": kline_data["t"],
                  "close_time": kline_data["T"],
                  "symbol": kline_data["s"],
                  "open_price": float(kline_data["o"]),
                  "close_price": float(kline_data["c"]),
                  "high_price": float(kline_data["h"]),
                  "low_price": float(kline_data["l"])}
    
    return price_data

if __name__ == "__main__":


    test_ewma = np.array([1, 2, 3, 4, 5])

    avg = np.average(test_ewma)
    smoothing_alpha = 0
    ewma = EWMA(test_ewma, smoothing_alpha)

    print(f"avg: {avg}, alpha: {smoothing_alpha}, EWMA: {ewma}")
