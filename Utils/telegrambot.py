import sys
import os
sys.path.append(os.path.abspath(__file__ + "/../.."))

import secret_keys
import telegram


def send_telegram_message(message):
    bot = telegram.Bot(token=secret_keys.telegram_bot_token)

    try:
        bot.send_message(chat_id=secret_keys.telegram_chat_id, text=message)
    except telegram.error.TelegramError as e:
        print(e.message)
        pass


if __name__ == "__main__":
    bot = telegram.Bot(token=secret_keys.telegram_bot_token)

    print(bot.get_updates()[0])
    print(f"Chat ID: {bot.get_updates()[0]['message']['chat']['id']}")